/* ----------------------------------------------------------------------------
 *         Cortex-M3 dummy handlers
 * ----------------------------------------------------------------------------
 * Copyright (c) 2014, Sebastian Holtermann
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following condition is met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer below.
 *
 * DISCLAIMER: THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDERS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ----------------------------------------------------------------------------
 */

#ifdef __cplusplus
extern "C" {
#endif

/** @brief Halts in an endless while loop */
static
void
__halt ( void )
{
	while ( 1 );
}

/** @brief Does nothing */
static
void
__empty ( void )
{
}

/* Cortex-M3 core handlers */
void NMI_Handler       (void) __attribute__ ((weak, alias("__halt")));
void HardFault_Handler (void) __attribute__ ((weak, alias("__halt")));
void MemManage_Handler (void) __attribute__ ((weak, alias("__halt")));
void BusFault_Handler  (void) __attribute__ ((weak, alias("__halt")));
void UsageFault_Handler(void) __attribute__ ((weak, alias("__halt")));
void DebugMon_Handler  (void) __attribute__ ((weak, alias("__halt")));
void SVC_Handler       (void) __attribute__ ((weak, alias("__halt")));
void PendSV_Handler    (void) __attribute__ ((weak, alias("__halt")));

void SysTick_Handler   (void) __attribute__ ((weak, alias("__empty")));

/* Peripherals handlers */
void SUPC_Handler       (void) __attribute__ ((weak, alias("__halt")));
void RSTC_Handler       (void) __attribute__ ((weak, alias("__halt")));
void RTC_Handler        (void) __attribute__ ((weak, alias("__halt")));
void RTT_Handler        (void) __attribute__ ((weak, alias("__halt")));
void WDT_Handler        (void) __attribute__ ((weak, alias("__halt")));
void PMC_Handler        (void) __attribute__ ((weak, alias("__halt")));
void EFC0_Handler       (void) __attribute__ ((weak, alias("__halt")));
void EFC1_Handler       (void) __attribute__ ((weak, alias("__halt")));
void UART_Handler       (void) __attribute__ ((weak, alias("__halt")));
void SMC_Handler        (void) __attribute__ ((weak, alias("__halt")));
#ifdef _SAM3XA_SDRAMC_INSTANCE_
void SDRAMC_Handler     (void) __attribute__ ((weak, alias("__halt")));
#endif
void PIOA_Handler       (void) __attribute__ ((weak, alias("__halt")));
void PIOB_Handler       (void) __attribute__ ((weak, alias("__halt")));
void PIOC_Handler       (void) __attribute__ ((weak, alias("__halt")));
void PIOD_Handler       (void) __attribute__ ((weak, alias("__halt")));
#ifdef _SAM3XA_PIOE_INSTANCE_
void PIOE_Handler       (void) __attribute__ ((weak, alias("__halt")));
#endif
#ifdef _SAM3XA_PIOF_INSTANCE_
void PIOF_Handler       (void) __attribute__ ((weak, alias("__halt")));
#endif
void USART0_Handler     (void) __attribute__ ((weak, alias("__halt")));
void USART1_Handler     (void) __attribute__ ((weak, alias("__halt")));
void USART2_Handler     (void) __attribute__ ((weak, alias("__halt")));
void USART3_Handler     (void) __attribute__ ((weak, alias("__halt")));
void HSMCI_Handler      (void) __attribute__ ((weak, alias("__halt")));
void TWI0_Handler       (void) __attribute__ ((weak, alias("__halt")));
void TWI1_Handler       (void) __attribute__ ((weak, alias("__halt")));
void SPI0_Handler       (void) __attribute__ ((weak, alias("__halt")));
#ifdef _SAM3XA_SPI1_INSTANCE_
void SPI1_Handler       (void) __attribute__ ((weak, alias("__halt")));
#endif
void SSC_Handler        (void) __attribute__ ((weak, alias("__halt")));
void TC0_Handler        (void) __attribute__ ((weak, alias("__halt")));
void TC1_Handler        (void) __attribute__ ((weak, alias("__halt")));
void TC2_Handler        (void) __attribute__ ((weak, alias("__halt")));
void TC3_Handler        (void) __attribute__ ((weak, alias("__halt")));
void TC4_Handler        (void) __attribute__ ((weak, alias("__halt")));
void TC5_Handler        (void) __attribute__ ((weak, alias("__halt")));
void TC6_Handler        (void) __attribute__ ((weak, alias("__halt")));
void TC7_Handler        (void) __attribute__ ((weak, alias("__halt")));
void TC8_Handler        (void) __attribute__ ((weak, alias("__halt")));
void PWM_Handler        (void) __attribute__ ((weak, alias("__halt")));
void ADC_Handler        (void) __attribute__ ((weak, alias("__halt")));
void DACC_Handler       (void) __attribute__ ((weak, alias("__halt")));
void DMAC_Handler       (void) __attribute__ ((weak, alias("__halt")));
void UOTGHS_Handler     (void) __attribute__ ((weak, alias("__halt")));
void TRNG_Handler       (void) __attribute__ ((weak, alias("__halt")));
void EMAC_Handler       (void) __attribute__ ((weak, alias("__halt")));
void CAN0_Handler       (void) __attribute__ ((weak, alias("__halt")));
void CAN1_Handler       (void) __attribute__ ((weak, alias("__halt")));


#ifdef __cplusplus
}
#endif

