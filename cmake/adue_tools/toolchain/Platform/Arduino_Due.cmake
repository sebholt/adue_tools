

include ( CMakeParseArguments )

#=============================================================================#
# [PUBLIC/USER]
# see documentation at top
#=============================================================================#
function ( GENERATE_ARDUINO_DUE_FIRMWARE INPUT_NAME )

    message ( STATUS "Generating ${INPUT_NAME}" )

    cmake_parse_arguments ( INPUT
        "" # Options
        "" # One value keywords
        "SOURCES;HEADERS" # Multi value keywords
        ${ARGN} )

    required_variables ( VARS INPUT_SOURCES MSG "must define SOURCES for target ${INPUT_NAME}")

    set ( SOURCES_ALL  ${INPUT_HEADERS} )
    list ( APPEND SOURCES_ALL ${INPUT_SOURCES} )
    list ( APPEND SOURCES_ALL ${INPUT_HEADERS} )
    # Add system sources
    list ( APPEND SOURCES_ALL ${ARDUINO_DUE_SOURCE_DIR}/cortex_handlers.c )
    list ( APPEND SOURCES_ALL ${ARDUINO_DUE_SOURCE_DIR}/startup_sam3xa.c )
    list ( APPEND SOURCES_ALL ${ARDUINO_DUE_SOURCE_DIR}/system_sam3xa.c )

    set ( LIBS_ALL )
    set ( COMPILE_FLAGS )
    set ( LINK_FLAGS "${ARDUINO_DUE_LINKER_FLAGS}" )
    if ( ARDUINO_DUE_LINK_NEWLIB_LIBC )
        # TODO: broken
        #list ( APPEND LIBS_ALL /usr/lib/arm-none-eabi/lib/armv7-m/libc.a )
    endif()
    if ( ARDUINO_DUE_LINK_NEWLIB_LIBSTDC++ )
        # TODO: broken
        #list ( APPEND LIBS_ALL /usr/lib/arm-none-eabi/lib/armv7-m/libc.a )
    endif()

    setup_arduino_due_target ( ${INPUT_NAME} "${SOURCES_ALL}" "${LIBS_ALL}" "${COMPILE_FLAGS}" "${LINK_FLAGS}" )
    setup_arduino_due_upload( ${INPUT_NAME} )

endfunction()



#=============================================================================#
# [PRIVATE/INTERNAL]
#
# setup_arduino_due_target(TARGET_NAME SOURCES_ALL LIBS_ALL COMPILE_FLAGS LINK_FLAGS MANUAL)
#
#        TARGET_NAME   - Target name
#        SOURCES_ALL   - All sources
#        LIBS_ALL      - All libraries
#        COMPILE_FLAGS - Compile flags
#        LINK_FLAGS    - Linker flags
#
# Creates an Arduino Due firmware target.
#
#=============================================================================#
function ( setup_arduino_due_target
    TARGET_NAME
    SOURCES_ALL
    LIBS_ALL
    COMPILE_FLAGS
    LINK_FLAGS
)
    # Local variables
    set ( TARGET_PATH ${CMAKE_CURRENT_BINARY_DIR}/${TARGET_NAME} )
    set ( TARGET_ELF ${TARGET_PATH}.elf )
    set ( TARGET_EEP ${TARGET_PATH}.eep )
    set ( TARGET_HEX ${TARGET_PATH}.hex )

    add_executable ( ${TARGET_NAME} ${SOURCES_ALL} )

    # Register flags (compile/link)
    set_target_properties ( ${TARGET_NAME} PROPERTIES
        SUFFIX ".elf"
        COMPILE_FLAGS "${COMPILE_FLAGS}"
        LINK_FLAGS "${LINK_FLAGS}"
    )
    # Link libraries
    target_link_libraries ( ${TARGET_NAME} ${LIBS_ALL} )

    # Convert firmware image to HEX format
    separate_arguments ( OBJCOPY_ARGS UNIX_COMMAND "${CMAKE_OBJCOPY_HEX_FLAGS}" )
    list ( APPEND OBJCOPY_ARGS "-O;binary" )
    list ( APPEND OBJCOPY_ARGS "${TARGET_ELF}" )
    list ( APPEND OBJCOPY_ARGS "${TARGET_HEX}" )
    add_custom_command (
        TARGET ${TARGET_NAME} POST_BUILD
        COMMAND ${CMAKE_OBJCOPY} ARGS ${OBJCOPY_ARGS}
        COMMENT "Generating HEX image"
        VERBATIM )

    # Show HEX size
    add_custom_command (
        TARGET ${TARGET_NAME} POST_BUILD
        COMMAND ${CMAKE_OBJSIZE}
        ARGS ${ARDUINO_OBJSIZE_FLAGS}
            ${TARGET_PATH}.elf
        COMMENT "Object sizes"
        VERBATIM )

endfunction()



#=============================================================================#
# [PRIVATE/INTERNAL]
#
# setup_arduino_upload ( TARGET_NAME )
#
#        TARGET_NAME   - Target name
#
# Creates an Arduino Due firmware upload target
#
#=============================================================================#
function ( setup_arduino_due_upload TARGET_NAME )
    # Determine short port name for bossac
    string ( REPLACE "/dev/" "" BOSSAC_PORT ${ARDUINO_DUE_PORT} )

    # Firmware hex files
    set ( TARGET_HEX "${CMAKE_CURRENT_BINARY_DIR}/${TARGET_NAME}.hex" )
    set ( TARGET_UPLOAD "${TARGET_NAME}-upload" )

    # Upload to device
    add_custom_target (
        ${TARGET_UPLOAD}
        DEPENDS ${TARGET_NAME} )

    # Upload: Prepare with a device reset via stty
    add_custom_command (
        TARGET ${TARGET_UPLOAD} POST_BUILD
        COMMAND stty ARGS -F ${ARDUINO_DUE_PORT} cs8 1200 hupcl
        COMMENT "Resetting device before upload at: ${ARDUINO_DUE_PORT}"
        VERBATIM )

    separate_arguments ( BOSSAC_ARGS UNIX_COMMAND "${CMAKE_PROGRAMMER_BOSSAC_FLAGS}" )
    list ( APPEND BOSSAC_ARGS "--port=${BOSSAC_PORT}" )
    list ( APPEND BOSSAC_ARGS "${TARGET_HEX}" )
    # Upload: Do the actual upload
    add_custom_command (
        TARGET ${TARGET_UPLOAD} POST_BUILD
        COMMAND ${CMAKE_PROGRAMMER_BOSSAC} ARGS ${BOSSAC_ARGS}
        COMMENT "Uploading to device at: ${ARDUINO_DUE_PORT}"
        VERBATIM )

endfunction()


#=============================================================================#
# [PRIVATE/INTERNAL]
#
# required_variables(MSG msg VARS var1 var2 .. varN)
#
#        MSG  - Message to be displayed in case of error
#        VARS - List of variables names to check
#
# Ensure the specified variables are not empty, otherwise a fatal error is emmited.
#=============================================================================#
function ( REQUIRED_VARIABLES )
    cmake_parse_arguments ( INPUT "" "MSG" "VARS" ${ARGN} )
    error_for_unparsed ( INPUT )
    foreach ( VAR ${INPUT_VARS} )
        if  ( "${${VAR}}" STREQUAL "" )
            message ( FATAL_ERROR "${VAR} not set: ${INPUT_MSG}" )
        endif()
    endforeach()
endfunction()


#=============================================================================#
# [PRIVATE/INTERNAL]
#
# error_for_unparsed(PREFIX)
#
#        PREFIX - Prefix name
#
# Emit fatal error if there are unparsed argument from cmake_parse_arguments().
#=============================================================================#
function ( ERROR_FOR_UNPARSED PREFIX )
    set ( ARGS "${${PREFIX}_UNPARSED_ARGUMENTS}" )
    if ( NOT ( "${ARGS}" STREQUAL "" ) )
        message ( FATAL_ERROR "unparsed argument: ${ARGS}" )
    endif()
endfunction()
