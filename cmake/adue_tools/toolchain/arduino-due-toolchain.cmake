# Toolchain for the Arduino Due

set ( CMAKE_SYSTEM_NAME "Arduino_Due" )
set ( CMAKE_SYSTEM_PROCESSOR "cortex-m3" )
set ( CMAKE_SYSTEM_VERSION 1.0.0.0 )

# Add current directory to CMake Module path automatically
if ( EXISTS  ${CMAKE_CURRENT_LIST_DIR}/Platform/Arduino_Due.cmake )
    set ( CMAKE_MODULE_PATH  ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_LIST_DIR} )
endif()

# --- System Paths
if ( UNIX )
    include ( Platform/UnixPaths )
elseif ( WIN32 )
    include ( Platform/WindowsPaths )
endif()

# --- Constants
set ( ARDUINO_DUE_MCU
    "cortex-m3"
    CACHE INTERNAL "Arduino Due microcontroller unit" )
set ( ARDUINO_DUE_F_CPU
    "84000000"
    CACHE INTERNAL "Arduino Due CPU frequency" )
set ( ARDUINO_DUE_PORT
    "/dev/ttyACM0"
    CACHE FILEPATH "Arduino Due programming port" )

set ( ARDUINO_DUE_USB_PRODUCT_ID "0x003e" CACHE INTERNAL "Arduino Due numerical USB product id" )
set ( ARDUINO_DUE_USB_VENDOR_ID "0x2341" CACHE INTERNAL "Arduino Due numerical USB vendor id" )

# --- Compilers
set ( CMAKE_C_COMPILER "arm-none-eabi-gcc" )
set ( CMAKE_CXX_COMPILER "arm-none-eabi-g++" )
set ( CMAKE_ASM_COMPILER "arm-none-eabi-as" )
set ( CMAKE_OBJCOPY "arm-none-eabi-objcopy" CACHE INTERNAL "objcopy" )
set ( CMAKE_OBJDUMP "arm-none-eabi-objdump" CACHE INTERNAL "objdump" )
set ( CMAKE_OBJSIZE "arm-none-eabi-size" CACHE INTERNAL "objsize" )
set ( CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY )

# --- Programmer (bossa)
set ( CMAKE_PROGRAMMER_BOSSAC "bossac"
    CACHE INTERNAL "bossa command line programmer" )
#set ( CMAKE_PROGRAMMER_BOSSAC_FLAGS "--force_usb_port=false --erase --write --verify --boot=1 --reset"
#    CACHE INTERNAL "bossa command line programmer flags" )
set ( CMAKE_PROGRAMMER_BOSSAC_FLAGS "--info --arduino-erase --erase --write --verify --boot=1"
    CACHE INTERNAL "bossa command line programmer flags" )

# --- Directories and files
set ( ARDUINO_DUE_TOOLCHAIN_DIR
    "${CMAKE_CURRENT_LIST_DIR}/arduino-due-toolchain"
    CACHE INTERNAL "Arduino Due toolchain resources directory" )
set ( ARDUINO_DUE_INCLUDE_DIR
    "${ARDUINO_DUE_TOOLCHAIN_DIR}/include"
    CACHE INTERNAL "Arduino Due toolchain include directory" )
set ( ARDUINO_DUE_SOURCE_DIR
    "${ARDUINO_DUE_TOOLCHAIN_DIR}/source"
    CACHE INTERNAL "Arduino Due toolchain source directory" )
set ( ARDUINO_DUE_LDSCRIPT_DIR
    "${ARDUINO_DUE_TOOLCHAIN_DIR}/ldscripts"
    CACHE INTERNAL "Arduino Due toolchain ld scripts directory" )
set ( ARDUINO_DUE_LDSCRIPT
    "sam3x8e_flash.ld"
    CACHE INTERNAL "Arduino Due toolchain ld script" )

include_directories ( ${ARDUINO_DUE_INCLUDE_DIR} )


# --- Arduino Due compiler flags
set ( ARDUINO_DUE_COMMON_FLAGS "\
-D__SAM3X8E__ \
-DMCU=${ARDUINO_DUE_MCU} \
-DF_CPU=${ARDUINO_DUE_F_CPU} \
-mcpu=${ARDUINO_DUE_MCU} \
-mthumb \
-specs=nano.specs \
-specs=nosys.specs \
-ffunction-sections \
-fdata-sections \
--param max-inline-insns-single=500 \
-O2 \
-s \
-Wextra \
-Wunused \
-Wall \
"
CACHE INTERNAL "Arduino Due common flags"
)

set ( ARDUINO_DUE_LINKER_FLAGS  "\
-L${ARDUINO_DUE_LDSCRIPT_DIR} \
-T ${ARDUINO_DUE_LDSCRIPT} \
-Wl,--Map=arduino_due.map \
-Wl,--cref \
-Wl,--check-sections \
-Wl,--gc-sections \
-Wl,--entry=Reset_Handler \
-Wl,--unresolved-symbols=report-all \
-Wl,--warn-common \
-Wl,--warn-section-align \
-Wl,--warn-unresolved-symbols \
-Wl,--as-needed \
"
CACHE INTERNAL "Arduino Due linker flags"
)

set ( ARDUINO_DUE_C_FLAGS "\
${ARDUINO_DUE_COMMON_FLAGS} \
-Wstrict-prototypes \
"
CACHE INTERNAL "Arduino Due C flags"
)

set ( ARDUINO_DUE_CXX_FLAGS "\
${ARDUINO_DUE_COMMON_FLAGS} \
-nostdlib \
-fno-threadsafe-statics \
-fno-exceptions \
-fno-rtti \
-Wno-register \
"
CACHE INTERNAL "Arduino Due C++ flags"
)

# --- Compiler flags
set ( CMAKE_C_FLAGS "${ARDUINO_DUE_C_FLAGS}" CACHE INTERNAL "C compiler flags" )
set ( CMAKE_CXX_FLAGS "${ARDUINO_DUE_CXX_FLAGS}" CACHE INTERNAL "C++ compiler flags" )
set ( CMAKE_ASM_FLAGS "${ARDUINO_DUE_COMMON_FLAGS}" CACHE INTERNAL "ASM compiler flags" )
