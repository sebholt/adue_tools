/// Arduino Due EFrog controller implementation.
/// \copyright See LICENSE-efrog.txt file

#pragma once

#include <cstdint>
#include <sam3x8e.h>

namespace adue::pwm
{

/// @brief TC channel controller
///
class Channel
{
    public:
    void
    init ( std::uint8_t index_n )
    {
        _channel = &PWM->PWM_CH_NUM[ index_n ];
        _channel_bit = ( 1 << index_n );
        _index = index_n;
    }

    PwmCh_num *
    channel () const
    {
        return _channel;
    }

    std::uint8_t
    channel_bit () const
    {
        return _channel_bit;
    }

    /// @brief Channel index
    std::uint8_t
    index () const
    {
        return _index;
    }

    // -- Registers

    std::uint32_t
    mode () const
    {
        return channel ()->PWM_CMR;
    }

    void
    set_mode ( std::uint32_t mode_n )
    {
        channel ()->PWM_CMR = mode_n;
    }

    // -- Enable / Disable

    bool
    is_enabled () const
    {
        return ( ( PWM->PWM_SR & channel_bit () ) != 0 );
    }

    void
    enable ()
    {
        PWM->PWM_ENA = channel_bit ();
    }

    void
    disable ()
    {
        PWM->PWM_DIS = channel_bit ();
    }

    // -- Counter

    std::uint32_t
    counter () const
    {
        return channel ()->PWM_CCNT;
    }

    // -- Period

    std::uint32_t
    period () const
    {
        return channel ()->PWM_CPRD;
    }

    void
    set_period ( std::uint32_t period_n )
    {
        channel ()->PWM_CPRD = period_n;
    }

    void
    set_period_update ( std::uint32_t period_n )
    {
        channel ()->PWM_CPRDUPD = period_n;
    }

    // -- Duty

    std::uint32_t
    duty () const
    {
        return channel ()->PWM_CDTY;
    }

    void
    set_duty ( std::uint32_t duty_n )
    {
        channel ()->PWM_CDTY = duty_n;
    }

    void
    set_duty_update ( std::uint32_t duty_n )
    {
        channel ()->PWM_CDTYUPD = duty_n;
    }

    // -- Dead time

    std::uint32_t
    dead_times () const
    {
        return channel ()->PWM_DT;
    }

    std::uint16_t
    dead_time_low () const
    {
        return static_cast< std::uint16_t > ( dead_times () >> 16 );
    }

    std::uint16_t
    dead_time_high () const
    {
        return static_cast< std::uint16_t > ( dead_times () );
    }

    void
    set_dead_times ( std::uint32_t times_n )
    {
        channel ()->PWM_DT = times_n;
    }

    void
    set_dead_times ( std::uint16_t low_n, std::uint16_t high_n )
    {
        set_dead_times ( std::uint32_t ( high_n ) &
                         ( std::uint32_t ( low_n ) << 16 ) );
    }

    void
    set_dead_times_update ( std::uint32_t times_n )
    {
        channel ()->PWM_DTUPD = times_n;
    }

    void
    set_dead_times_update ( std::uint16_t low_n, std::uint16_t high_n )
    {
        set_dead_times_update ( std::uint32_t ( high_n ) &
                                ( std::uint32_t ( low_n ) << 16 ) );
    }

    // -- Output override

    std::uint32_t
    output_override_high_bits () const
    {
        return ( 1u << ( 0 + index () ) );
    }

    std::uint32_t
    output_override_low_bits () const
    {
        return ( 1u << ( 16 + index () ) );
    }

    std::uint32_t
    output_override_bits () const
    {
        return ( output_override_high_bits () | output_override_low_bits () );
    }

    // -- Interrupt

    void
    interrupt_enable ()
    {
        PWM->PWM_IER1 = channel_bit ();
    }

    void
    interrupt_disable ()
    {
        PWM->PWM_IDR1 = channel_bit ();
    }

    private:
    PwmCh_num * _channel;
    std::uint8_t _channel_bit;
    std::uint8_t _index;
};

/// @brief TC channel controller (instance free)
///
template < std::uint8_t IDX_ >
class Channel_T
{
    public:
    static constexpr PwmCh_num *
    channel ()
    {
        return &PWM->PWM_CH_NUM[ IDX_ ];
    }

    static constexpr std::uint8_t
    index ()
    {
        return IDX_;
    }

    static constexpr std::uint8_t
    channel_bit ()
    {
        return std::uint8_t ( 1 << index () );
    }

    // -- Registers

    static std::uint32_t
    mode ()
    {
        return channel ()->PWM_CMR;
    }

    static void
    set_mode ( std::uint32_t mode_n )
    {
        channel ()->PWM_CMR = mode_n;
    }

    // -- Enable / Disable

    static bool
    is_enabled ()
    {
        return ( ( PWM->PWM_SR & channel_bit () ) != 0 );
    }

    static void
    enable ()
    {
        PWM->PWM_ENA = channel_bit ();
    }

    static void
    disable ()
    {
        PWM->PWM_DIS = channel_bit ();
    }

    // -- Counter

    static std::uint32_t
    counter ()
    {
        return channel ()->PWM_CCNT;
    }

    // -- Period

    static std::uint32_t
    period ()
    {
        return channel ()->PWM_CPRD;
    }

    static void
    set_period ( std::uint32_t period_n )
    {
        channel ()->PWM_CPRD = period_n;
    }

    static void
    set_period_update ( std::uint32_t period_n )
    {
        channel ()->PWM_CPRDUPD = period_n;
    }

    // -- Duty

    static std::uint32_t
    duty ()
    {
        return channel ()->PWM_CDTY;
    }

    static void
    set_duty ( std::uint32_t duty_n )
    {
        channel ()->PWM_CDTY = duty_n;
    }

    static void
    set_duty_update ( std::uint32_t duty_n )
    {
        channel ()->PWM_CDTYUPD = duty_n;
    }

    // -- Dead time

    static std::uint32_t
    dead_times ()
    {
        return channel ()->PWM_DT;
    }

    static std::uint16_t
    dead_time_low ()
    {
        return static_cast< std::uint16_t > ( dead_times () >> 16 );
    }

    static std::uint16_t
    dead_time_high ()
    {
        return static_cast< std::uint16_t > ( dead_times () );
    }

    static void
    set_dead_times ( std::uint32_t times_n )
    {
        channel ()->PWM_DT = times_n;
    }

    static void
    set_dead_times ( std::uint16_t low_n, std::uint16_t high_n )
    {
        set_dead_times ( std::uint32_t ( high_n ) &
                         ( std::uint32_t ( low_n ) << 16 ) );
    }

    static void
    set_dead_times_update ( std::uint32_t times_n )
    {
        channel ()->PWM_DTUPD = times_n;
    }

    static void
    set_dead_times_update ( std::uint16_t low_n, std::uint16_t high_n )
    {
        set_dead_times_update ( std::uint32_t ( high_n ) &
                                ( std::uint32_t ( low_n ) << 16 ) );
    }

    // -- Output override

    static constexpr std::uint32_t
    output_override_high_bits ()
    {
        return ( 1u << ( 0 + index () ) );
    }

    static constexpr std::uint32_t
    output_override_low_bits ()
    {
        return ( 1u << ( 16 + index () ) );
    }

    static constexpr std::uint32_t
    output_override_bits ()
    {
        return ( output_override_high_bits () | output_override_low_bits () );
    }

    // -- Interrupt

    static void
    interrupt_enable ()
    {
        PWM->PWM_IER1 = channel_bit ();
    }

    static void
    interrupt_disable ()
    {
        PWM->PWM_IDR1 = channel_bit ();
    }
};

} // namespace adue::pwm
