/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#pragma once

#include <cstdint>

namespace adue
{

// --- Function declarations

// -- uint16

inline void
get_uint16_8le ( std::uint16_t & val16_n, const std::uint8_t * vals8_n )
{
    std::uint8_t * ptr8 ( (std::uint8_t *)( &val16_n ) );
    ptr8[ 0 ] = vals8_n[ 0 ];
    ptr8[ 1 ] = vals8_n[ 1 ];
}

inline std::uint16_t
read_uint16_8le ( const std::uint8_t * vals8_n )
{
    std::uint16_t res;
    get_uint16_8le ( res, vals8_n );
    return res;
}

inline void
set_8le_uint16 ( std::uint8_t * vals8_n, const std::uint16_t & val16_n )
{
    const std::uint8_t * ptr8 ( (const std::uint8_t *)&val16_n );
    vals8_n[ 0 ] = ptr8[ 0 ];
    vals8_n[ 1 ] = ptr8[ 1 ];
}

// -- uint32

inline void
get_uint32_8le ( std::uint32_t & val32_n, const std::uint8_t * vals8_n )
{
    std::uint8_t * ptr8 ( (std::uint8_t *)( &val32_n ) );
    ptr8[ 0 ] = vals8_n[ 0 ];
    ptr8[ 1 ] = vals8_n[ 1 ];
    ptr8[ 2 ] = vals8_n[ 2 ];
    ptr8[ 3 ] = vals8_n[ 3 ];
}

inline std::uint32_t
read_uint32_8le ( const std::uint8_t * vals8_n )
{
    std::uint32_t res;
    get_uint32_8le ( res, vals8_n );
    return res;
}

inline void
set_8le_uint32 ( std::uint8_t * vals8_n, const std::uint32_t & val32_n )
{
    const std::uint8_t * ptr8 ( (const std::uint8_t *)&val32_n );
    vals8_n[ 0 ] = ptr8[ 0 ];
    vals8_n[ 1 ] = ptr8[ 1 ];
    vals8_n[ 2 ] = ptr8[ 2 ];
    vals8_n[ 3 ] = ptr8[ 3 ];
}

// -- uint64

inline void
get_uint64_8le ( std::uint64_t & val64_n, const std::uint8_t * vals8_n )
{
    std::uint8_t * ptr8 ( (std::uint8_t *)( &val64_n ) );
    ptr8[ 0 ] = vals8_n[ 0 ];
    ptr8[ 1 ] = vals8_n[ 1 ];
    ptr8[ 2 ] = vals8_n[ 2 ];
    ptr8[ 3 ] = vals8_n[ 3 ];
    ptr8[ 4 ] = vals8_n[ 4 ];
    ptr8[ 5 ] = vals8_n[ 5 ];
    ptr8[ 6 ] = vals8_n[ 6 ];
    ptr8[ 7 ] = vals8_n[ 7 ];
}

inline std::uint64_t
read_uint64_8le ( const std::uint8_t * vals8_n )
{
    std::uint64_t res;
    get_uint64_8le ( res, vals8_n );
    return res;
}

inline void
set_8le_uint64 ( std::uint8_t * vals8_n, const std::uint64_t & val64_n )
{
    const std::uint8_t * ptr8 ( (const std::uint8_t *)&val64_n );
    vals8_n[ 0 ] = ptr8[ 0 ];
    vals8_n[ 1 ] = ptr8[ 1 ];
    vals8_n[ 2 ] = ptr8[ 2 ];
    vals8_n[ 3 ] = ptr8[ 3 ];
    vals8_n[ 4 ] = ptr8[ 4 ];
    vals8_n[ 5 ] = ptr8[ 5 ];
    vals8_n[ 6 ] = ptr8[ 6 ];
    vals8_n[ 7 ] = ptr8[ 7 ];
}

} // namespace adue
