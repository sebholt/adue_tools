/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#pragma once

#include <cstdint>

namespace adue::usb
{

// Enum definitions

enum USB_Descriptor_Types
{
    DESC_TYPE_DEVICE = 0x01,
    DESC_TYPE_CONFIGURATION = 0x02,
    DESC_TYPE_STRING = 0x03,
    DESC_TYPE_INTERFACE = 0x04,
    DESC_TYPE_ENDPOINT = 0x05,
};

enum USB_Setup_Requests
{
    SETUP_GET_STATUS = 0x00,
    SETUP_CLEAR_FEATURE = 0x01,
    SETUP_SET_FEATURE = 0x03,
    SETUP_SET_ADDRESS = 0x05,
    SETUP_GET_DESCRIPTOR = 0x06,
    SETUP_SET_DESCRIPTOR = 0x07,
    SETUP_GET_CONFIGURATION = 0x08,
    SETUP_SET_CONFIGURATION = 0x09,
    SETUP_GET_INTERFACE = 0x0A,
    SETUP_SET_INTERFACE = 0x11,
    SETUP_SYNCH_FRAME = 0x12
};

// USB setup request bits
const std::uint8_t SETUP_REQ_BITS_RECIPIENT = 0x1F;
const std::uint8_t SETUP_REQ_RECIPIENT_DEVICE = 0x00;
const std::uint8_t SETUP_REQ_RECIPIENT_INTERFACE = 0x01;
const std::uint8_t SETUP_REQ_RECIPIENT_ENDPOINT = 0x02;
const std::uint8_t SETUP_REQ_RECIPIENT_OTHER = 0x03;

const std::uint8_t SETUP_REQ_BITS_TYPE = 0x60;
const std::uint8_t SETUP_REQ_TYPE_STANDARD = 0x00;
const std::uint8_t SETUP_REQ_TYPE_CLASS = 0x20;
const std::uint8_t SETUP_REQ_TYPE_VENDOR = 0x40;
const std::uint8_t SETUP_REQ_TYPE_RESERVED = 0x60;

const std::uint8_t SETUP_REQ_BITS_DIR = 0x80;
const std::uint8_t SETUP_REQ_DIR_HOST_TO_DEVICE = 0x00;
const std::uint8_t SETUP_REQ_DIR_DEVICE_TO_HOST = 0x80;

// USB Descriptor bits

const std::uint8_t DESC_ATTR_BIT_SELF_POWERED = 0x40;
const std::uint8_t DESC_ATTR_BIT_BUS_POWERED = 0x80;
const std::uint8_t DESC_ATTR_BIT_REMOTE_WAKEUP = 0x80;

const std::uint8_t DESC_EPT_ADDR_NUMBER_POS = 0;
const std::uint8_t DESC_EPT_ADDR_NUMBER_MASK = 0x07;
const std::uint8_t DESC_EPT_ADDR_DIR_IN = ( 0x01 << 7 );
const std::uint8_t DESC_EPT_ADDR_DIR_OUT = ( 0x00 << 7 );

const std::uint8_t DESC_EPT_ATTR_TRANSFER_CONTROL = 0x00;
const std::uint8_t DESC_EPT_ATTR_TRANSFER_ISOCHRONOUS = 0x01;
const std::uint8_t DESC_EPT_ATTR_TRANSFER_BULK = 0x02;
const std::uint8_t DESC_EPT_ATTR_TRANSFER_INTERRUPT = 0x03;

// USB specification type definitions

struct Descriptor_Device
{
    std::uint8_t bLength;
    std::uint8_t bDescriptorType;
    std::uint8_t bcdUSB_0;
    std::uint8_t bcdUSB_1;
    std::uint8_t bDeviceClass;
    std::uint8_t bDeviceSubClass;
    std::uint8_t bDeviceProtocol;
    std::uint8_t bMaxPacketSize;
    std::uint8_t idVendor_0;
    std::uint8_t idVendor_1;
    std::uint8_t idProduct_0;
    std::uint8_t idProduct_1;
    std::uint8_t bcdDevice_0;
    std::uint8_t bcdDevice_1;
    std::uint8_t iManufacturer;
    std::uint8_t iProduct;
    std::uint8_t iSerialNumber;
    std::uint8_t bNumConfigurations;
};

struct Descriptor_Configuration
{
    std::uint8_t bLength;
    std::uint8_t bDescriptorType;
    std::uint8_t wTotalLenght_0;
    std::uint8_t wTotalLenght_1;
    std::uint8_t bNumInterfaces;
    std::uint8_t bConfigurationValue;
    std::uint8_t iConfiguration;
    std::uint8_t bmAttributes;
    std::uint8_t bMaxPower;
};

struct Descriptor_Interface
{
    std::uint8_t bLength;
    std::uint8_t bDescriptorType;
    std::uint8_t bInterfaceNumber;
    std::uint8_t bAlternateSetting;
    std::uint8_t bNumEndpoints;
    std::uint8_t bInterfaceClass;
    std::uint8_t bInterfaceSubClass;
    std::uint8_t bInterfaceProtocol;
    std::uint8_t iInterface;
};

struct Descriptor_Endpoint
{
    std::uint8_t bLength;
    std::uint8_t bDescriptorType;
    std::uint8_t bEndpointAddress;
    std::uint8_t bmAttributes;
    std::uint8_t wMaxPacketSize_0;
    std::uint8_t wMaxPacketSize_1;
    std::uint8_t bInterval;
};

struct Descriptor_String_Header
{
    std::uint8_t bLength;
    std::uint8_t bDescriptorType;
};

struct Setup_Package
{
    std::uint8_t bmRequestType;
    std::uint8_t bRequest;
    std::uint8_t wValue_0;
    std::uint8_t wValue_1;
    std::uint8_t wIndex_0;
    std::uint8_t wIndex_1;
    std::uint8_t wLength_0;
    std::uint8_t wLength_1;
};

} // namespace usb
