/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#pragma once

#include "api.hpp"
#include "endpoint_fifo.hpp"
#include "usb_device.hpp"
#include <cstdint>
#include <adue/data_ref.hpp>

namespace adue
{

class USB_Bulk_IO
{
    // Public types
    public:
    enum String_Index
    {
        SI_MANUFACTURER,
        SI_PRODUCT,
        SI_SERIAL,
        SI_CONFIG,
        SI_IFACE
    };

    static const unsigned int _num_strings = 5;

    // Public methods
    public:
    void
    init ();

    void
    set_desc_string ( unsigned int str_idx_n, const adue::Data_Ref & dref_n );

    void
    set_desc_string ( unsigned int str_idx_n,
                      const std::uint8_t * data_n,
                      std::uint32_t size_n );

    void
    enable ();

    void
    disable ();

    bool
    is_enabled () const;

    bool
    is_connnected () const;

    bool
    read_bank_poll ();

    std::uint32_t
    read_bank_bytes () const;

    volatile std::uint8_t *
    read_bank ();

    void
    read_bank_clear ();

    bool
    write_bank_available ();

    std::uint32_t
    write_bank_size () const;

    std::uint32_t
    write_some ( const std::uint8_t * data_n,
                 std::uint32_t size_n,
                 bool force_flush_n = true );

    void
    write_all ( const std::uint8_t * data_n,
                std::uint32_t size_n,
                bool force_flush_n = true );

    volatile std::uint8_t *
    write_bank ();

    bool
    write_flushable ();

    void
    write_flush ();

    void
    write_flush_on_demand ();

    // Private methods
    private:
    static void
    interrupt_handler_anon ( void * context_n );

    void
    interrupt_handler ();

    void
    descriptors_init ();

    void
    descriptors_update ();

    bool
    send_descriptor ( const usb::Setup_Package & setup_n );

    void
    init_endpoint_control ();

    void
    init_endpoint_out ();

    void
    init_endpoint_in ();

    // Private attributes
    private:
    static const unsigned int ep_ctl_bank_size = 64;
    static const unsigned int ep_out_bank_size = 512;
    static const unsigned int ep_in_bank_size = 512;

    bool _is_enabled;
    volatile std::uint8_t _configuration;
    volatile std::uint8_t _interface;

    adue::usb::Endpoint_FIFO< 0 > _ep_ctl_fifo;
    adue::usb::Endpoint_FIFO< 1 > _ep_out_fifo;
    adue::usb::Endpoint_FIFO< 2 > _ep_in_fifo;

    // Device descriptors
    usb::Descriptor_Device _desc_device;
    usb::Descriptor_Configuration _desc_configuration;
    usb::Descriptor_Interface _desc_interface;
    usb::Descriptor_Endpoint _desc_ept_out;
    usb::Descriptor_Endpoint _desc_ept_in;

    usb::Descriptor_String_Header _desc_string_header;
    std::uint8_t _desc_string_langs[ 2 ];

    adue::Data_Ref _str_dref[ _num_strings ];
};

inline void
USB_Bulk_IO::set_desc_string ( unsigned int str_idx_n,
                               const std::uint8_t * data_n,
                               std::uint32_t size_n )
{
    set_desc_string ( str_idx_n, adue::Data_Ref ( data_n, size_n ) );
}

inline bool
USB_Bulk_IO::is_enabled () const
{
    return _is_enabled;
}

inline bool
USB_Bulk_IO::is_connnected () const
{
    return ( _configuration != 0 );
}

inline bool
USB_Bulk_IO::read_bank_poll ()
{
    if ( _ep_out_fifo.read_ready () ) {
        _ep_out_fifo.read_ready_acknowledge ();
        return true;
    }
    return false;
}

inline std::uint32_t
USB_Bulk_IO::read_bank_bytes () const
{
    return _ep_out_fifo.read_bank_bytes ();
}

inline volatile std::uint8_t *
USB_Bulk_IO::read_bank ()
{
    return _ep_out_fifo.bank_begin ();
}

inline void
USB_Bulk_IO::read_bank_clear ()
{
    return _ep_out_fifo.read_bank_clear ();
}

inline bool
USB_Bulk_IO::write_bank_available ()
{
    return _ep_in_fifo.write_bank_good ();
}

inline std::uint32_t
USB_Bulk_IO::write_bank_size () const
{
    return _ep_in_fifo.bank_size ();
}

inline std::uint32_t
USB_Bulk_IO::write_some ( const std::uint8_t * data_n,
                          std::uint32_t size_n,
                          bool force_flush_n )
{
    return _ep_in_fifo.write_some ( data_n, size_n, force_flush_n );
}

inline void
USB_Bulk_IO::write_all ( const std::uint8_t * data_n,
                         std::uint32_t size_n,
                         bool force_flush_n )
{
    _ep_in_fifo.write_all ( data_n, size_n, force_flush_n );
}

inline volatile std::uint8_t *
USB_Bulk_IO::write_bank ()
{
    return _ep_in_fifo.bank_begin ();
}

inline bool
USB_Bulk_IO::write_flushable ()
{
    return _ep_in_fifo.write_flushable ();
}

inline void
USB_Bulk_IO::write_flush ()
{
    _ep_in_fifo.write_flush ();
}

inline void
USB_Bulk_IO::write_flush_on_demand ()
{
    _ep_in_fifo.write_flush_on_demand ();
}

} // namespace adue
