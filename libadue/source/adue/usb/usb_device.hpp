/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#pragma once

#include <sam3x8e.h>

namespace adue
{

// Type definitions

using USB_Interrupt_Handler = void ( * ) ( void * context_n );

// Variable declarations

extern void * _usb_interrupt_handler_context;
extern adue::USB_Interrupt_Handler _usb_interrupt_handler;

// Function declarations

/// @brief Current usb interrupt handler
adue::USB_Interrupt_Handler
usb_interrupt_handler ();

/// @brief Sets the usb interrupt handler
void
set_usb_interrupt_handler ( void * context_n,
                            adue::USB_Interrupt_Handler handler_n );

/// @brief Clears the usb interrupt handler
void
clear_usb_interrupt_handler ();

// Function definitions

inline adue::USB_Interrupt_Handler
usb_interrupt_handler ()
{
    return _usb_interrupt_handler;
}

} // namespace adue
