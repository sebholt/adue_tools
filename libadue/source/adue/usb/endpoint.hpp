/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#pragma once

#include <sam3x8e.h>

namespace adue::usb
{

/// @brief USB endpoint registers interface class
///
template < unsigned int EP_INDEX >
class Endpoint
{
    public:
    // --- Registers

    static constexpr RwReg &
    config_register ()
    {
        return UOTGHS->UOTGHS_DEVEPTCFG[ EP_INDEX ];
    }

    static constexpr RoReg &
    status_register ()
    {
        return UOTGHS->UOTGHS_DEVEPTISR[ EP_INDEX ];
    }

    static constexpr WoReg &
    clear_register ()
    {
        return UOTGHS->UOTGHS_DEVEPTICR[ EP_INDEX ];
    }

    static constexpr WoReg &
    set_register ()
    {
        return UOTGHS->UOTGHS_DEVEPTIFR[ EP_INDEX ];
    }

    static constexpr RoReg &
    mask_register ()
    {
        return UOTGHS->UOTGHS_DEVEPTIMR[ EP_INDEX ];
    }

    static constexpr WoReg &
    enable_register ()
    {
        return UOTGHS->UOTGHS_DEVEPTIER[ EP_INDEX ];
    }

    static constexpr WoReg &
    disable_register ()
    {
        return UOTGHS->UOTGHS_DEVEPTIDR[ EP_INDEX ];
    }

    /// @brief Data bank
    static constexpr volatile std::uint8_t *
    bank ()
    {
        return reinterpret_cast< volatile std::uint8_t ( * )[ 0x8000 ] > (
            UOTGHS_RAM_ADDR )[ EP_INDEX ];
    }
};

} // namespace adue
