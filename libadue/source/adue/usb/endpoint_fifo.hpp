/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#pragma once

#include <sam3x8e.h>

namespace adue::usb
{

template < unsigned int EP_IDX >
class Endpoint_FIFO
{
    // Public methods
    public:
    void
    init_control ( std::uint32_t bank_size_n );

    void
    init_read ();

    void
    init_write ( std::uint32_t bank_size_n );

    bool
    read_ready () const;

    void
    read_ready_acknowledge ();

    std::uint32_t
    read_bank_bytes () const;

    bool
    read_bank_valid ();

    void
    read_bank_clear ();

    void
    read_byte ( std::uint8_t & byte_n );

    std::uint32_t
    read_some ( std::uint8_t * data_n, std::uint32_t size_n );

    bool
    write_ready () const;

    void
    write_ready_wait ();

    void
    write_ready_acknowledge ();

    bool
    write_bank_good () const;

    void
    write_bank_good_wait ();

    void
    write_byte ( std::uint8_t byte_n );

    /// @return Number of bytes written
    /// @arg force_flush_n Flush even when the bank is not full
    std::uint32_t
    write_some ( const std::uint8_t * data_n,
                 std::uint32_t size_n,
                 bool force_flush_n = true );

    /// @brief Blocking write
    /// @arg force_flush_n Flush even when the bank is not full
    void
    write_all ( const std::uint8_t * data_n,
                std::uint32_t size_n,
                bool force_flush_n = true );

    bool
    write_flushable ();

    void
    write_flush ();

    void
    write_flush_on_demand ();

    std::uint32_t
    io_pos () const;

    std::uint32_t
    bank_size () const;

    std::uint32_t
    available_bank_bytes () const;

    volatile std::uint8_t *
    bank_begin ();

    // Private methods
    private:
    std::uint32_t _io_pos;
    std::uint32_t _bank_size;
};

template < unsigned int EP_IDX >
inline void
Endpoint_FIFO< EP_IDX >::init_control ( std::uint32_t bank_size_n )
{
    _io_pos = 0;
    _bank_size = bank_size_n;
}

template < unsigned int EP_IDX >
inline void
Endpoint_FIFO< EP_IDX >::init_read ()
{
    _io_pos = 0;
    _bank_size = 0;
}

template < unsigned int EP_IDX >
inline void
Endpoint_FIFO< EP_IDX >::init_write ( std::uint32_t bank_size_n )
{
    _io_pos = 0;
    _bank_size = bank_size_n;
}

template < unsigned int EP_IDX >
inline std::uint32_t
Endpoint_FIFO< EP_IDX >::io_pos () const
{
    return _io_pos;
}

template < unsigned int EP_IDX >
inline std::uint32_t
Endpoint_FIFO< EP_IDX >::bank_size () const
{
    return _bank_size;
}

template < unsigned int EP_IDX >
inline std::uint32_t
Endpoint_FIFO< EP_IDX >::available_bank_bytes () const
{
    return ( _bank_size - _io_pos );
}

template < unsigned int EP_IDX >
inline volatile std::uint8_t *
Endpoint_FIFO< EP_IDX >::bank_begin ()
{
    return (
        ( (volatile std::uint8_t ( * )[ 0x8000 ])UOTGHS_RAM_ADDR )[ EP_IDX ] );
}

template < unsigned int EP_IDX >
inline bool
Endpoint_FIFO< EP_IDX >::read_ready () const
{
    return ( ( UOTGHS->UOTGHS_DEVEPTISR[ EP_IDX ] & UOTGHS_DEVEPTISR_RXOUTI ) !=
             0 );
}

template < unsigned int EP_IDX >
inline void
Endpoint_FIFO< EP_IDX >::read_ready_acknowledge ()
{
    UOTGHS->UOTGHS_DEVEPTICR[ EP_IDX ] = UOTGHS_DEVEPTICR_RXOUTIC;
    _bank_size =
        ( ( UOTGHS->UOTGHS_DEVEPTISR[ EP_IDX ] & UOTGHS_DEVEPTISR_BYCT_Msk ) >>
          UOTGHS_DEVEPTISR_BYCT_Pos );
}

template < unsigned int EP_IDX >
inline std::uint32_t
Endpoint_FIFO< EP_IDX >::read_bank_bytes () const
{
    return _bank_size;
}

template < unsigned int EP_IDX >
inline bool
Endpoint_FIFO< EP_IDX >::read_bank_valid ()
{
    return ( ( UOTGHS->UOTGHS_DEVEPTIMR[ EP_IDX ] &
               UOTGHS_DEVEPTIMR_FIFOCON ) != 0 );
}

template < unsigned int EP_IDX >
inline void
Endpoint_FIFO< EP_IDX >::read_bank_clear ()
{
    UOTGHS->UOTGHS_DEVEPTIDR[ EP_IDX ] = UOTGHS_DEVEPTIDR_FIFOCONC;
}

template < unsigned int EP_IDX >
inline void
Endpoint_FIFO< EP_IDX >::read_byte ( std::uint8_t & byte_n )
{
    byte_n = *( bank_begin () + _io_pos );
    ++_io_pos;
}

template < unsigned int EP_IDX >
inline std::uint32_t
Endpoint_FIFO< EP_IDX >::read_some ( std::uint8_t * data_n,
                                         std::uint32_t size_n )
{
    const std::uint32_t num_avail ( available_bank_bytes () );
    if ( size_n > num_avail ) {
        size_n = num_avail;
    }
    volatile std::uint8_t * rptr ( bank_begin () + _io_pos );
    for ( unsigned int ii = 0; ii != size_n; ++ii ) {
        data_n[ ii ] = rptr[ ii ];
    }
    _io_pos += size_n;
    return size_n;
}

template < unsigned int EP_IDX >
inline bool
Endpoint_FIFO< EP_IDX >::write_ready () const
{
    return ( ( UOTGHS->UOTGHS_DEVEPTISR[ EP_IDX ] & UOTGHS_DEVEPTISR_TXINI ) !=
             0 );
}

template < unsigned int EP_IDX >
inline void
Endpoint_FIFO< EP_IDX >::write_ready_wait ()
{
    while ( !write_ready () )
        ;
}

template < unsigned int EP_IDX >
inline void
Endpoint_FIFO< EP_IDX >::write_ready_acknowledge ()
{
    UOTGHS->UOTGHS_DEVEPTICR[ EP_IDX ] = UOTGHS_DEVEPTICR_TXINIC;
}

template < unsigned int EP_IDX >
inline bool
Endpoint_FIFO< EP_IDX >::write_bank_good () const
{
    return ( ( UOTGHS->UOTGHS_DEVEPTIMR[ EP_IDX ] &
               UOTGHS_DEVEPTIMR_FIFOCON ) != 0 );
}

template < unsigned int EP_IDX >
inline void
Endpoint_FIFO< EP_IDX >::write_byte ( std::uint8_t byte_n )
{
    *( bank_begin () + _io_pos ) = byte_n;
    ++_io_pos;
    if ( _io_pos == _bank_size ) {
        write_flush ();
    }
}

template < unsigned int EP_IDX >
std::uint32_t
Endpoint_FIFO< EP_IDX >::write_some ( const std::uint8_t * data_n,
                                          std::uint32_t size_n,
                                          bool force_flush_n )
{
    {
        const std::uint32_t free_bytes ( available_bank_bytes () );
        if ( size_n > free_bytes ) {
            size_n = free_bytes;
        }
    }
    {
        volatile std::uint8_t * wptr ( bank_begin () + _io_pos );
        for ( unsigned int ii = 0; ii != size_n; ++ii ) {
            wptr[ ii ] = data_n[ ii ];
        }
        _io_pos += size_n;
    }
    // Flush on demand
    if ( _io_pos == _bank_size ) {
        force_flush_n = true;
    }
    if ( force_flush_n ) {
        write_flush ();
    }

    return size_n;
}

template < unsigned int EP_IDX >
void
Endpoint_FIFO< EP_IDX >::write_all ( const std::uint8_t * data_n,
                                         std::uint32_t size_n,
                                         bool force_flush_n )
{
    const std::uint8_t * rpos ( data_n );
    do {
        if ( !write_bank_good () ) {
            write_ready_wait ();
        }
        const std::uint32_t num ( write_some ( rpos, size_n, force_flush_n ) );
        size_n -= num;
        if ( size_n == 0 ) {
            break;
        }
        rpos += num;
    } while ( true );
}

template < unsigned int EP_IDX >
inline bool
Endpoint_FIFO< EP_IDX >::write_flushable ()
{
    return ( _io_pos != 0 );
}

template < unsigned int EP_IDX >
inline void
Endpoint_FIFO< EP_IDX >::write_flush ()
{
    if ( EP_IDX == 0 ) {
        // Control endpoint doesn't use FIFO flush bit
        UOTGHS->UOTGHS_DEVEPTICR[ EP_IDX ] = UOTGHS_DEVEPTICR_TXINIC;
    } else {
        UOTGHS->UOTGHS_DEVEPTICR[ EP_IDX ] = UOTGHS_DEVEPTICR_TXINIC;
        UOTGHS->UOTGHS_DEVEPTIDR[ EP_IDX ] = UOTGHS_DEVEPTIDR_FIFOCONC;
    }
    _io_pos = 0;
}

template < unsigned int EP_IDX >
inline void
Endpoint_FIFO< EP_IDX >::write_flush_on_demand ()
{
    if ( write_flushable () ) {
        write_flush ();
    }
}

} // namespace adue
