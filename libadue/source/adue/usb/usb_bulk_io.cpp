/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#include <adue/pmc.hpp>
#include <adue/serialize.hpp>
#include <adue/usb/usb_bulk_io.hpp>

namespace adue
{

static const std::uint8_t desc_str_idx_manufacturer = 1;
static const std::uint8_t desc_str_idx_product = 2;
static const std::uint8_t desc_str_idx_serial = 3;
static const std::uint8_t desc_str_idx_configuration = 4;
static const std::uint8_t desc_str_idx_interface = 5;

void
USB_Bulk_IO::init ()
{
    {
        // Detach
        UOTGHS->UOTGHS_DEVCTRL |= UOTGHS_DEVCTRL_DETACH;

        // Disable USB controller
        UOTGHS->UOTGHS_CTRL &= ~UOTGHS_CTRL_USBE;
        // Freeze controller clock
        UOTGHS->UOTGHS_CTRL |= UOTGHS_CTRL_FRZCLK;

        // Disable USB-On-The-Go interrupts
        NVIC_DisableIRQ ( (IRQn_Type)ID_UOTGHS );

        // Disable USB-On-The-Go peripheral clock
        pmc_disable_periph_clk ( ID_UOTGHS );
    }

    _is_enabled = false;
    _configuration = 0;
    _interface = 0;

    _ep_ctl_fifo.init_control ( ep_ctl_bank_size );
    _ep_out_fifo.init_read ();
    _ep_in_fifo.init_write ( ep_in_bank_size );

    // Clear string references
    for ( unsigned int ii = 0; ii < _num_strings; ++ii ) {
        _str_dref[ ii ].clear ();
    }

    descriptors_init ();

    adue::set_usb_interrupt_handler (
        this, &adue::USB_Bulk_IO::interrupt_handler_anon );
}

void
USB_Bulk_IO::descriptors_init ()
{
    // Device descriptor
    {
        _desc_device.bLength = 18;
        _desc_device.bDescriptorType = usb::DESC_TYPE_DEVICE;
        _desc_device.bcdUSB_0 = 0x00;
        _desc_device.bcdUSB_1 = 0x02;
        _desc_device.bDeviceClass = 0x00;
        _desc_device.bDeviceSubClass = 0x00;
        _desc_device.bDeviceProtocol = 0x00;
        _desc_device.bMaxPacketSize = ep_ctl_bank_size;
        adue::set_8le_uint16 ( &_desc_device.idVendor_0, USB_ID_VENDOR );
        adue::set_8le_uint16 ( &_desc_device.idProduct_0, USB_ID_PRODUCT );
        _desc_device.bcdDevice_0 = 0x00;
        _desc_device.bcdDevice_1 = 0x01;
        _desc_device.iManufacturer = 0;
        _desc_device.iProduct = 0;
        _desc_device.iSerialNumber = 0;
        _desc_device.bNumConfigurations = 1;
    }
    // Configuration descriptor
    {
        _desc_configuration.bLength = sizeof ( usb::Descriptor_Configuration );
        _desc_configuration.bDescriptorType = usb::DESC_TYPE_CONFIGURATION;
        adue::set_8le_uint16 (
            &_desc_configuration.wTotalLenght_0,
            sizeof ( usb::Descriptor_Configuration ) +
                sizeof ( usb::Descriptor_Interface ) +
                ( sizeof ( usb::Descriptor_Endpoint ) * 2 ) );
        _desc_configuration.bNumInterfaces = 1;
        _desc_configuration.bConfigurationValue = 1;
        _desc_configuration.iConfiguration = 0;
        _desc_configuration.bmAttributes =
            usb::DESC_ATTR_BIT_SELF_POWERED | usb::DESC_ATTR_BIT_BUS_POWERED;
        _desc_configuration.bMaxPower = 250;
    }
    // Interface descriptor
    {
        _desc_interface.bLength = sizeof ( usb::Descriptor_Interface );
        _desc_interface.bDescriptorType = usb::DESC_TYPE_INTERFACE;
        _desc_interface.bInterfaceNumber = 0;
        _desc_interface.bAlternateSetting = 0;
        _desc_interface.bNumEndpoints = 2;
        _desc_interface.bInterfaceClass = 0xFF;
        _desc_interface.bInterfaceSubClass = 0xFF;
        _desc_interface.bInterfaceProtocol = 0xFF;
        _desc_interface.iInterface = 0;
    }
    // Endpoint descriptor: Host OUT
    {
        _desc_ept_out.bLength = sizeof ( usb::Descriptor_Endpoint );
        _desc_ept_out.bDescriptorType = usb::DESC_TYPE_ENDPOINT;
        _desc_ept_out.bEndpointAddress =
            ( 0x01 << usb::DESC_EPT_ADDR_NUMBER_POS ) |
            usb::DESC_EPT_ADDR_DIR_OUT;
        _desc_ept_out.bmAttributes = usb::DESC_EPT_ATTR_TRANSFER_BULK;
        adue::set_8le_uint16 ( &_desc_ept_out.wMaxPacketSize_0,
                               ep_out_bank_size );
        _desc_ept_out.bInterval = 1;
    }
    // Endpoint descriptor: Host IN
    {
        _desc_ept_in.bLength = sizeof ( usb::Descriptor_Endpoint );
        _desc_ept_in.bDescriptorType = usb::DESC_TYPE_ENDPOINT;
        _desc_ept_in.bEndpointAddress =
            ( 0x02 << usb::DESC_EPT_ADDR_NUMBER_POS ) |
            usb::DESC_EPT_ADDR_DIR_IN;
        _desc_ept_in.bmAttributes = usb::DESC_EPT_ATTR_TRANSFER_BULK;
        adue::set_8le_uint16 ( &_desc_ept_in.wMaxPacketSize_0,
                               ep_in_bank_size );
        _desc_ept_in.bInterval = 1;
    }

    // String descriptors
    _desc_string_header.bLength = 2;
    _desc_string_header.bDescriptorType = usb::DESC_TYPE_STRING;
    _desc_string_langs[ 0 ] = 0x09; // wLANGID[0] low
    _desc_string_langs[ 1 ] = 0x04; // wLANGID[0] high
}

void
USB_Bulk_IO::descriptors_update ()
{
    // Device descriptor
    {
        std::uint32_t str_idx ( 0 );
        if ( _str_dref[ SI_MANUFACTURER ].is_valid () ) {
            str_idx = desc_str_idx_manufacturer;
        }
        _desc_device.iManufacturer = str_idx;
    }
    {
        std::uint32_t str_idx ( 0 );
        if ( _str_dref[ SI_PRODUCT ].is_valid () ) {
            str_idx = desc_str_idx_product;
        }
        _desc_device.iProduct = str_idx;
    }
    {
        std::uint32_t str_idx ( 0 );
        if ( _str_dref[ SI_SERIAL ].is_valid () ) {
            str_idx = desc_str_idx_serial;
        }
        _desc_device.iSerialNumber = str_idx;
    }
    // Configuration descriptor
    {
        std::uint32_t str_idx ( 0 );
        if ( _str_dref[ SI_CONFIG ].is_valid () ) {
            str_idx = desc_str_idx_configuration;
        }
        _desc_configuration.iConfiguration = str_idx;
    }
    // Interface descriptor
    {
        std::uint32_t str_idx ( 0 );
        if ( _str_dref[ SI_IFACE ].is_valid () ) {
            str_idx = desc_str_idx_interface;
        }
        _desc_interface.iInterface = str_idx;
    }
}

void
USB_Bulk_IO::set_desc_string ( unsigned int str_idx_n,
                               const adue::Data_Ref & dref_n )
{
    if ( !is_enabled () && ( str_idx_n < _num_strings ) ) {
        adue::Data_Ref & str_ref ( _str_dref[ str_idx_n ] );
        if ( str_ref != dref_n ) {
            str_ref = dref_n;
            descriptors_update ();
        }
    }
}

void
USB_Bulk_IO::enable ()
{
    if ( !_is_enabled ) {
        _is_enabled = true;

        // Enable USB-On-The-Go peripheral clock
        pmc_enable_periph_clk ( ID_UOTGHS );
        // Enable UPLL clock
        pmc_enable_upll_clock ();
        // Switch to use UPLL clock for USB
        pmc_switch_udpck_to_upllck ( 0 ); // div=0+1
        // Enable USB clock
        pmc_enable_udpck ();

        // Disable USB host/device mode auto selection
        UOTGHS->UOTGHS_CTRL &= ~UOTGHS_CTRL_UIDE;
        // Enable USB device mode
        UOTGHS->UOTGHS_CTRL |= UOTGHS_CTRL_UIMOD;

        // Reset USB-On-The-Go Pad
        UOTGHS->UOTGHS_CTRL &= ~UOTGHS_CTRL_OTGPADE;
        UOTGHS->UOTGHS_CTRL |= UOTGHS_CTRL_OTGPADE;

        // Disable forced low speed
        UOTGHS->UOTGHS_DEVCTRL &= ~UOTGHS_DEVCTRL_LS;
        // Select speed
        UOTGHS->UOTGHS_DEVCTRL =
            ( UOTGHS->UOTGHS_DEVCTRL & UOTGHS_DEVCTRL_SPDCONF_Msk ) |
            UOTGHS_DEVCTRL_SPDCONF_NORMAL;

        // Enable USB-On-The-Go
        UOTGHS->UOTGHS_CTRL |= UOTGHS_CTRL_USBE;
        // Unfreeze controller clock
        UOTGHS->UOTGHS_CTRL &= ~UOTGHS_CTRL_FRZCLK;
        // Wait until clock is usable
        while ( ( UOTGHS->UOTGHS_SR & UOTGHS_SR_CLKUSABLE ) == 0 )
            ;

        // Enable interrupt on "End of reset"
        UOTGHS->UOTGHS_DEVIER |= UOTGHS_DEVIER_EORSTES;

        // Enable USB-On-The-Go interrupts
        NVIC_EnableIRQ ( (IRQn_Type)ID_UOTGHS );

        // Attach device
        UOTGHS->UOTGHS_DEVCTRL &= ~UOTGHS_DEVCTRL_DETACH;
    }
}

void
USB_Bulk_IO::disable ()
{
    if ( _is_enabled ) {
        // Disable USB-On-The-Go interrupts
        NVIC_DisableIRQ ( (IRQn_Type)ID_UOTGHS );

        // Detach device
        UOTGHS->UOTGHS_DEVCTRL |= UOTGHS_DEVCTRL_DETACH;
        // Disable interrupt on "End of reset"
        UOTGHS->UOTGHS_DEVIDR |= UOTGHS_DEVIER_EORSTES;

        // Disable USB controller
        UOTGHS->UOTGHS_CTRL &= ~UOTGHS_CTRL_USBE;
        // Freeze controller clock
        UOTGHS->UOTGHS_CTRL |= UOTGHS_CTRL_FRZCLK;
        // Disable USB-On-The-Go peripheral clock
        pmc_disable_periph_clk ( ID_UOTGHS );

        _is_enabled = false;
        _configuration = 0;
        _interface = 0;
    }
}

void
USB_Bulk_IO::interrupt_handler_anon ( void * context_n )
{
    adue::USB_Bulk_IO * inst (
        reinterpret_cast< adue::USB_Bulk_IO * > ( context_n ) );
    inst->interrupt_handler ();
}

inline void
USB_Bulk_IO::interrupt_handler ()
{
    // End of reset interrupt
    if ( ( UOTGHS->UOTGHS_DEVISR & UOTGHS_DEVISR_EORST ) != 0 ) {
        // Reset USB address to 0
        UOTGHS->UOTGHS_DEVCTRL &= ~UOTGHS_DEVCTRL_UADD_Msk;
        // Enable address 0
        UOTGHS->UOTGHS_DEVCTRL |= UOTGHS_DEVCTRL_ADDEN;

        // Configure EP 0
        {
            init_endpoint_control ();

            // Enable endpoint setup receive interrupt
            UOTGHS->UOTGHS_DEVEPTIER[ 0 ] = UOTGHS_DEVEPTIER_RXSTPES;
            // Enable endpoint interrupts
            UOTGHS->UOTGHS_DEVIER = UOTGHS_DEVIER_PEP_0;
        }

        _configuration = 0;

        // Acknowledge end of reset interrupt
        UOTGHS->UOTGHS_DEVICR = UOTGHS_DEVICR_EORSTC;
    }

    // Endpoint 0 (control) interrupt
    if ( ( UOTGHS->UOTGHS_DEVISR & UOTGHS_DEVISR_PEP_0 ) != 0 ) {

        // Check if the setup package is complete
        if ( ( UOTGHS->UOTGHS_DEVEPTISR[ 0 ] & UOTGHS_DEVEPTISR_RXSTPI ) !=
             0 ) {
            bool send_good ( true );
            bool send_stall ( true );

            // Read setup package
            usb::Setup_Package pck_setup;
            _ep_ctl_fifo.read_some ( (std::uint8_t *)&pck_setup, 8 );

            // Clear setup package
            UOTGHS->UOTGHS_DEVEPTICR[ 0 ] = UOTGHS_DEVEPTICR_RXSTPIC;

            if ( ( pck_setup.bmRequestType & usb::SETUP_REQ_BITS_DIR ) ==
                 usb::SETUP_REQ_DIR_HOST_TO_DEVICE ) {
                // Host OUT request
                _ep_ctl_fifo.write_flush ();
            } else {
                // Host IN request
                _ep_ctl_fifo.write_ready_wait ();
            }

            if ( pck_setup.bRequest == usb::SETUP_GET_STATUS ) {
                if ( pck_setup.bmRequestType == 0 ) {
                    // Device status
                    // Not self powered, not sending wake up requests
                    _ep_ctl_fifo.write_byte ( 0 );
                    _ep_ctl_fifo.write_byte ( 0 );
                } else {
                    // Endpoint status
                    _ep_ctl_fifo.write_byte ( 0 );
                    _ep_ctl_fifo.write_byte ( 0 );
                }
            } else if ( pck_setup.bRequest == usb::SETUP_CLEAR_FEATURE ) {

            } else if ( pck_setup.bRequest == usb::SETUP_SET_FEATURE ) {

            } else if ( pck_setup.bRequest == usb::SETUP_SET_ADDRESS ) {
                // Load address
                {
                    std::uint32_t addr_bits ( pck_setup.wValue_0 );
                    addr_bits = ( addr_bits << UOTGHS_DEVCTRL_UADD_Pos );
                    addr_bits &= UOTGHS_DEVCTRL_UADD_Msk;
                    UOTGHS->UOTGHS_DEVCTRL =
                        ( UOTGHS->UOTGHS_DEVCTRL & UOTGHS_DEVCTRL_UADD_Msk ) |
                        addr_bits;
                }
                // Disable address
                UOTGHS->UOTGHS_DEVCTRL &= ~UOTGHS_DEVCTRL_ADDEN;
                // Send ZLP
                _ep_ctl_fifo.write_ready_wait ();
                _ep_ctl_fifo.write_flush ();
                // Enable address
                UOTGHS->UOTGHS_DEVCTRL |= UOTGHS_DEVCTRL_ADDEN;

                send_good = false;
                send_stall = false;
            } else if ( pck_setup.bRequest == usb::SETUP_GET_DESCRIPTOR ) {

                send_good = send_descriptor ( pck_setup );

            } else if ( pck_setup.bRequest == usb::SETUP_SET_DESCRIPTOR ) {

                send_good = false;

            } else if ( pck_setup.bRequest == usb::SETUP_GET_CONFIGURATION ) {

                _ep_ctl_fifo.write_byte ( _configuration );

            } else if ( pck_setup.bRequest == usb::SETUP_SET_CONFIGURATION ) {
                if ( ( pck_setup.bmRequestType &
                       usb::SETUP_REQ_BITS_RECIPIENT ) ==
                     usb::SETUP_REQ_RECIPIENT_DEVICE ) {
                    // Configuration selected
                    init_endpoint_out ();
                    init_endpoint_in ();
                    _configuration = pck_setup.wValue_0;
                } else {
                    send_good = false;
                }

            } else if ( pck_setup.bRequest == usb::SETUP_GET_INTERFACE ) {

                _ep_ctl_fifo.write_byte ( _interface );

            } else if ( pck_setup.bRequest == usb::SETUP_SET_INTERFACE ) {

                _interface = pck_setup.wValue_0;
            }

            if ( send_good ) {
                _ep_ctl_fifo.write_flush ();
            } else if ( send_stall ) {
                UOTGHS->UOTGHS_DEVEPTIER[ 0 ] = UOTGHS_DEVEPTIER_STALLRQS;
            }
        }
    }
}

bool
USB_Bulk_IO::send_descriptor ( const usb::Setup_Package & setup_n )
{
    unsigned int num_drefs ( 0 );
    adue::Data_Ref send_dref[ 4 ];

    const std::uint8_t desc_type ( setup_n.wValue_1 );
    if ( desc_type == usb::DESC_TYPE_DEVICE ) {
        // --- Device descriptor
        num_drefs = 1;
        send_dref[ 0 ].set ( (const std::uint8_t *)&_desc_device,
                             _desc_device.bLength );

    } else if ( desc_type == usb::DESC_TYPE_CONFIGURATION ) {
        // --- Configuration descriptor
        num_drefs = 4;
        send_dref[ 0 ].set ( (const std::uint8_t *)( &_desc_configuration ),
                             _desc_configuration.bLength );
        send_dref[ 1 ].set ( (const std::uint8_t *)( &_desc_interface ),
                             _desc_interface.bLength );
        send_dref[ 2 ].set ( (const std::uint8_t *)( &_desc_ept_out ),
                             _desc_ept_out.bLength );
        send_dref[ 3 ].set ( (const std::uint8_t *)( &_desc_ept_in ),
                             _desc_ept_in.bLength );

    } else if ( desc_type == usb::DESC_TYPE_STRING ) {
        // --- String descriptor
        if ( setup_n.wValue_0 == 0 ) {
            // -- Available languages descriptor
            num_drefs = 2;
            send_dref[ 0 ].set ( (const std::uint8_t *)( &_desc_string_header ),
                                 2 );
            send_dref[ 1 ].set ( &_desc_string_langs[ 0 ], 2 );

            // Adjust descriptor length
            _desc_string_header.bLength = 4;
        } else {
            // -- Actual string
            if ( setup_n.wIndex_0 == _desc_string_langs[ 0 ] &&
                 setup_n.wIndex_1 == _desc_string_langs[ 1 ] ) {
                // Default language requested
                switch ( setup_n.wValue_0 ) {
                case desc_str_idx_manufacturer:
                    send_dref[ 1 ] = _str_dref[ SI_MANUFACTURER ];
                    break;
                case desc_str_idx_product:
                    send_dref[ 1 ] = _str_dref[ SI_PRODUCT ];
                    break;
                case desc_str_idx_serial:
                    send_dref[ 1 ] = _str_dref[ SI_SERIAL ];
                    break;
                case desc_str_idx_configuration:
                    send_dref[ 1 ] = _str_dref[ SI_CONFIG ];
                    break;
                case desc_str_idx_interface:
                    send_dref[ 1 ] = _str_dref[ SI_IFACE ];
                    break;
                default:
                    break;
                }
            }
            if ( send_dref[ 1 ].is_valid () ) {
                num_drefs = 2;
                send_dref[ 0 ].set (
                    (const std::uint8_t *)( &_desc_string_header ), 2 );

                // Adjust descriptor length
                _desc_string_header.bLength = 2 + send_dref[ 1 ].size ();
            }
        }
    }

    // Write data from the data references
    if ( num_drefs != 0 ) {
        std::uint32_t num_bytes_sendable (
            adue::read_uint16_8le ( &setup_n.wLength_0 ) );
        adue::Data_Ref * it_cur ( &send_dref[ 0 ] );
        adue::Data_Ref * it_end ( it_cur + num_drefs );
        while ( ( it_cur != it_end ) && ( num_bytes_sendable != 0 ) ) {
            {
                std::uint32_t num_send ( it_cur->size () );
                if ( num_send > num_bytes_sendable ) {
                    num_send = num_bytes_sendable;
                }
                num_bytes_sendable -= num_send;
                _ep_ctl_fifo.write_all ( it_cur->data (), num_send, false );
            }
            ++it_cur;
        }
        return true;
    }

    return false;
}

void
USB_Bulk_IO::init_endpoint_control ()
{
    // Control endpoint
    const std::uint32_t ept_idx ( 0 );

    // Load endpoint configuration
    UOTGHS->UOTGHS_DEVEPTCFG[ ept_idx ] =
        ( UOTGHS_DEVEPTCFG_EPTYPE_CTRL | UOTGHS_DEVEPTCFG_EPBK_1_BANK |
          UOTGHS_DEVEPTCFG_EPSIZE_64_BYTE | UOTGHS_DEVEPTCFG_ALLOC );

    // Enable endpoint
    UOTGHS->UOTGHS_DEVEPT |= ( UOTGHS_DEVEPT_EPEN0 << ept_idx );
}

void
USB_Bulk_IO::init_endpoint_out ()
{
    // Host OUT endpoint
    const std::uint32_t ept_idx ( 1 );
    // Load endpoint configuration
    UOTGHS->UOTGHS_DEVEPTCFG[ ept_idx ] =
        ( UOTGHS_DEVEPTCFG_EPTYPE_BLK | UOTGHS_DEVEPTCFG_EPBK_3_BANK |
          UOTGHS_DEVEPTCFG_EPSIZE_512_BYTE | UOTGHS_DEVEPTCFG_EPDIR_OUT |
          UOTGHS_DEVEPTCFG_ALLOC );

    // Enable endpoint
    UOTGHS->UOTGHS_DEVEPT |= ( UOTGHS_DEVEPT_EPEN0 << ept_idx );
}

void
USB_Bulk_IO::init_endpoint_in ()
{
    // Host IN endpoint
    const std::uint32_t ept_idx ( 2 );

    // Load endpoint configuration
    UOTGHS->UOTGHS_DEVEPTCFG[ ept_idx ] =
        ( UOTGHS_DEVEPTCFG_EPTYPE_BLK | UOTGHS_DEVEPTCFG_EPBK_3_BANK |
          UOTGHS_DEVEPTCFG_EPSIZE_512_BYTE | UOTGHS_DEVEPTCFG_EPDIR_IN |
          UOTGHS_DEVEPTCFG_ALLOC );

    // Enable endpoint
    UOTGHS->UOTGHS_DEVEPT |= ( UOTGHS_DEVEPT_EPEN0 << ept_idx );
}

} // namespace adue
