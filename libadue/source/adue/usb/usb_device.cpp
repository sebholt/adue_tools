/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#include <adue/usb/usb_device.hpp>

namespace
{

void
_usb_interrupt_handler_noop ( void * context_n )
{
    (void)context_n;
}

} // namespace

namespace adue
{

void * _usb_interrupt_handler_context = nullptr;
adue::USB_Interrupt_Handler _usb_interrupt_handler =
    &_usb_interrupt_handler_noop;

void
set_usb_interrupt_handler ( void * context_n,
                            adue::USB_Interrupt_Handler handler_n )
{
    _usb_interrupt_handler_context = context_n;
    _usb_interrupt_handler = handler_n;
}

void
clear_usb_interrupt_handler ()
{
    _usb_interrupt_handler_context = nullptr;
    _usb_interrupt_handler = &_usb_interrupt_handler_noop;
}

} // namespace adue

// -- Interrupt handlers

void
UOTGHS_Handler ( void )
{
    adue::_usb_interrupt_handler ( adue::_usb_interrupt_handler_context );
}
