/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#pragma once

#include <cstdint>
#include <adue/data_ref.hpp>
#include <adue/types.hpp>
#include <adue/usb/api.hpp>
#include <adue/usb/bulk_io/bank_control.hpp>
#include <adue/usb/bulk_io/bank_in.hpp>
#include <adue/usb/bulk_io/bank_out.hpp>
#include <adue/usb/endpoint.hpp>

namespace adue::usb::bulk_io
{

/// @brief Descriptor string type
enum class Descriptor_String
{
    MANUFACTURER,
    PRODUCT,
    SERIAL,
    CONFIG,
    INTERFACE,
    USER
};

/// @brief USB bulk io controller
///
class Controller
{
    public:
    // -- Types
    /// @brief Callback Type
    using Callback = void ( * ) ();

    enum class Setup_Type : std::uint8_t
    {
        NONE,
        WRITE,
        READ
    };

    enum class Setup_Write : std::uint8_t
    {
        NONE,
        BEGIN,
        READ_MORE,
        COMPLETE
    };

    enum class Setup_Read : std::uint8_t
    {
        NONE,
        BEGIN,
        WRITE_MORE,
        DONE
    };

    /// @brief Number of descriptor strings
    static constexpr const std::uint32_t num_desc_strings =
        static_cast< std::uint32_t > ( Descriptor_String::USER );

    public:
    void
    init ();

    void
    init_desc_string ( Descriptor_String index_n,
                       const adue::Data_Ref & dref_n );

    void
    init_desc_string ( Descriptor_String index_n,
                       const std::uint8_t * data_n,
                       std::uint32_t size_n )
    {
        init_desc_string ( index_n, adue::Data_Ref ( data_n, size_n ) );
    }

    // --- Enable / disable

    void
    enable ();

    void
    disable ();

    bool
    is_enabled () const
    {
        return ( _is_enabled != uint32_false );
    }

    // --- Attach / detach

    void
    attach ();

    void
    detach ();

    // --- Status

    bool
    is_vbus_connected () const
    {
        return ( ( UOTGHS->UOTGHS_SR & UOTGHS_SR_VBUS ) != 0 );
    }

    std::uint32_t
    configuration () const
    {
        return _configuration;
    }

    bool
    configuration_valid () const
    {
        return ( _configuration != 0 );
    }

    std::uint32_t
    interface () const
    {
        return _interface;
    }

    // --- Callbacks

    /// @brief Called after an end of reset was initialized by the host.
    ///
    /// - The device now has an address
    /// - The endpoints are enabled and reset
    /// - configuration() is 1
    /// - interface() is 0
    Callback const &
    callback_eor () const
    {
        return _callback_eor;
    }

    void
    set_callback_eor ( Callback callback_n )
    {
        _callback_eor = callback_n;
    }

    /// @brief Called after the configuration 1 was selected by the host
    ///
    /// - The endpoints are enabled and reset
    /// - configuration() is 1
    /// - interface() is 0
    Callback const &
    callback_config () const
    {
        return _callback_config;
    }

    void
    set_callback_config ( Callback callback_n )
    {
        _callback_config = callback_n;
    }

    /// @brief Called after the interface 0 was selected by the host.
    ///
    /// - The endpoints are enabled and reset
    /// - configuration() is 1
    /// - interface() is 0
    Callback const &
    callback_interface () const
    {
        return _callback_interface;
    }

    void
    set_callback_interface ( Callback callback_n )
    {
        _callback_interface = callback_n;
    }

    /// @brief Called when the start of a frame was detected
    Callback const &
    callback_sof () const
    {
        return _callback_sof;
    }

    void
    set_callback_sof ( Callback callback_n )
    {
        _callback_sof = callback_n;
    }

    // --- Endpoint banks

    bool
    banks_enabled () const
    {
        const std::uint32_t mask ( UOTGHS_DEVEPT_EPEN1 | UOTGHS_DEVEPT_EPEN2 );
        return ( ( UOTGHS->UOTGHS_DEVEPT & mask ) == mask );
    }

    /// @brief Host OUT bank interface
    constexpr Bank_Out &
    bank_out ()
    {
        return _bank_out;
    }

    /// @brief Host IN bank interface
    constexpr Bank_In &
    bank_in ()
    {
        return _bank_in;
    }

    // --- Interrupt event processing

    void
    interrupt_handler ( std::uint32_t isr_n )
    {
        // End of reset interrupt
        if ( ( isr_n & UOTGHS_DEVISR_EORST ) != 0 ) {
            // End of reset handling has highest priority
            process_end_of_reset ();
        }
        // Begin of frame interrupt
        if ( ( isr_n & UOTGHS_DEVISR_EORST ) != 0 ) {
            _callback_sof ();
        }
        // Endpoint 0 (control) interrupt
        if ( ( isr_n & UOTGHS_DEVISR_PEP_0 ) != 0 ) {
            process_control ();
        }
        // Endpoint 1 (OUT) interrupt
        if ( ( isr_n & UOTGHS_DEVISR_PEP_1 ) != 0 ) {
            _bank_out.process_interrupt ();
        }
        // Endpoint 2 (IN) interrupt
        if ( ( isr_n & UOTGHS_DEVISR_PEP_2 ) != 0 ) {
            _bank_in.process_interrupt ();
        }
    }

    bool
    poll_interrupt ()
    {
        std::uint32_t const isr = UOTGHS->UOTGHS_DEVISR;
        if ( ( isr & UOTGHS->UOTGHS_DEVIMR ) != 0 ) {
            interrupt_handler ( isr );
            return true;
        }
        return false;
    }

    private:
    void
    init_descriptors ();

    void
    setup_endpoints ();

    void
    process_end_of_reset ();

    void
    process_control ();

    bool
    process_control_write_begin ();

    void
    process_control_write_read_more ();

    void
    process_control_read_begin ();

    void
    process_control_read_begin_descriptor ();

    bool
    process_control_read_send_more ();

    bool
    process_control_read_send_more_descriptor ();

    void
    reset_data_endpoints ();

    Bank_Control &
    bank_control ()
    {
        return _bank_control;
    }

    private:
    std::uint32_t _is_enabled;
    std::uint32_t volatile _enable_address;
    std::uint32_t volatile _configuration;
    std::uint32_t volatile _interface;

    // -- Endpoint banks
    Bank_Control _bank_control;
    Bank_Out _bank_out;
    Bank_In _bank_in;

    // -- Callbacks
    Callback _callback_eor;
    Callback _callback_config;
    Callback _callback_interface;
    Callback _callback_sof;

    // -- Device setup
    struct
    {
        Setup_Type type;
        Setup_Write write;
        Setup_Read read;
        bool bad;
        usb::Setup_Package packet;

        // Device descriptors
        struct
        {
            usb::Descriptor_Device device;
            usb::Descriptor_Configuration configuration;
            usb::Descriptor_Interface interface;
            usb::Descriptor_Endpoint ept_out;
            usb::Descriptor_Endpoint ept_in;

            usb::Descriptor_String_Header string_header;
            std::uint8_t string_langs[ 2 ];

            adue::Data_Ref string_dref[ num_desc_strings ];
        } desc;

        // Device descriptor transfer status
        struct
        {
            std::uint32_t num_sendable;
            std::uint32_t cur_pos;
            adue::Data_Ref * cur;
            adue::Data_Ref * end;
            adue::Data_Ref dref[ 4 ];
        } desc_send;
    } _setup;
};

} // namespace adue::usb::bulk_io
