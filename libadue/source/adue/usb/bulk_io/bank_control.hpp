/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#pragma once

#include <cstdint>
#include <adue/usb/endpoint.hpp>

namespace adue::usb::bulk_io
{

// Forward declaration
class Controller;

/// @brief Control endpoint bank interface
class Bank_Control
{
    public:
    using UEP = adue::usb::Endpoint< 0 >;
    static constexpr const unsigned int bank_size = 64;

    std::uint32_t
    bytes_writable () const
    {
        return ( bank_size - ( _cur - UEP::bank () ) );
    }

    void
    read ( std::uint8_t * data_n, std::uint32_t size_n )
    {
        for ( std::uint32_t ii = 0; ii != size_n; ++ii ) {
            data_n[ ii ] = _cur[ ii ];
        }
        _cur += size_n;
    }

    void
    write_byte ( std::uint8_t byte_n )
    {
        *_cur = byte_n;
        ++_cur;
    }

    void
    write ( const std::uint8_t * data_n, std::uint32_t size_n )
    {
        for ( std::uint32_t ii = 0; ii != size_n; ++ii ) {
            _cur[ ii ] = data_n[ ii ];
        }
        _cur += size_n;
    }

    private:
    friend class Controller;

    void
    reset_data ()
    {
        _cur = UEP::bank ();
    }

    private:
    std::uint8_t volatile * _cur;
};

} // namespace adue::usb::bulk_io
