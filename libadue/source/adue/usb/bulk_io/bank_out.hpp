/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#pragma once

#include <cstdint>
#include <adue/usb/endpoint.hpp>

namespace adue::usb::bulk_io
{

// Forward declaration
class Controller;

/// @brief Host OUT endpoint bank interface
class Bank_Out
{
    public:
    /// @brief Callback Type
    using Callback = void ( * ) ();
    using UEP = adue::usb::Endpoint< 1 >;
    static constexpr const unsigned int bank_size = 512;

    /// @return true if the bank is enabled and usable
    static bool
    is_enabled ()
    {
        return ( ( UOTGHS->UOTGHS_DEVEPT & UOTGHS_DEVEPT_EPEN1 ) != 0 );
    }

    // -- Callback

    /// @brief Gets called when the bank becomes writable
    Callback const &
    callback () const
    {
        return _callback;
    }

    void
    set_callback ( Callback callback_n )
    {
        _callback = callback_n;
    }

    // -- Reading

    bool
    is_empty () const
    {
        return ( _cur == _end );
    }

    std::uint32_t
    bytes_available () const
    {
        return ( _end - _cur );
    }

    std::uint8_t
    read_byte ()
    {
        std::uint8_t res = *_cur;
        ++_cur;
        return res;
    }

    void
    read ( std::uint8_t * data_n, std::uint32_t size_n )
    {
        for ( std::uint32_t ii = 0; ii != size_n; ++ii ) {
            data_n[ ii ] = _cur[ ii ];
        }
        _cur += size_n;
    }

    std::uint8_t const volatile *
    read_ptr () const
    {
        return _cur;
    }

    void
    increment_read_ptr ( std::uint32_t delta_n = 1u )
    {
        _cur += delta_n;
    }

    bool
    release ();

    private:
    friend class Controller;

    void
    reset_data ()
    {
        _cur = UEP::bank ();
        _end = UEP::bank ();
    }

    void
    reset_endpoint ();

    void
    process_interrupt ();

    private:
    std::uint8_t const volatile * _cur;
    std::uint8_t const volatile * _end;
    Callback _callback;
};

} // namespace adue::usb::bulk_io
