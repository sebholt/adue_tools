/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#pragma once

#include <cstdint>
#include <adue/usb/endpoint.hpp>

namespace adue::usb::bulk_io
{

// Forward declaration
class Controller;

/// @brief Host IN endpoint bank interface
class Bank_In
{
    public:
    /// @brief Callback Type
    using Callback = void ( * ) ();
    using UEP = adue::usb::Endpoint< 2 >;
    static constexpr const unsigned int bank_size = 512;

    /// @return true if the bank is enabled and usable
    static bool
    is_enabled ()
    {
        return ( ( UOTGHS->UOTGHS_DEVEPT & UOTGHS_DEVEPT_EPEN2 ) != 0 );
    }

    // -- Callback

    /// @brief Gets called when new data arrives
    Callback const &
    callback () const
    {
        return _callback;
    }

    void
    set_callback ( Callback callback_n )
    {
        _callback = callback_n;
    }

    // -- Writing

    std::uint32_t
    bytes_available () const
    {
        return ( _end - _cur );
    }

    /// @brief Number of bytes already written to the bank
    std::uint32_t
    size () const
    {
        return ( _cur - UEP::bank () );
    }

    void
    write_byte ( std::uint8_t byte_n )
    {
        *_cur = byte_n;
        ++_cur;
    }

    void
    write ( const std::uint8_t * data_n, std::uint32_t size_n )
    {
        for ( std::uint32_t ii = 0; ii != size_n; ++ii ) {
            _cur[ ii ] = data_n[ ii ];
        }
        _cur += size_n;
    }

    std::uint8_t volatile *
    write_ptr () const
    {
        return _cur;
    }

    void
    increment_write_ptr ( std::uint32_t delta_n = 1 )
    {
        _cur += delta_n;
    }

    bool
    submit ();

    private:
    friend class Controller;

    void
    reset_data ()
    {
        _cur = UEP::bank ();
        _end = UEP::bank ();
    }

    void
    reset_endpoint ();

    void
    process_interrupt ();

    private:
    std::uint8_t volatile * _cur;
    std::uint8_t volatile * _end;
    Callback _callback;
};

} // namespace adue::usb::bulk_io
