/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#include <adue/usb/bulk_io/bank_out.hpp>

namespace adue::usb::bulk_io
{

void
Bank_Out::reset_endpoint ()
{
    if ( ( UOTGHS->UOTGHS_DEVEPT & UOTGHS_DEVEPT_EPEN1 ) != 0 ) {
        // Disable interrupt
        Bank_Out::UEP::disable_register () = UOTGHS_DEVEPTIDR_RXOUTEC;

        // Reset
        UOTGHS->UOTGHS_DEVEPT |= UOTGHS_DEVEPT_EPRST1;
        Bank_Out::UEP::enable_register () = UOTGHS_DEVEPTIER_RSTDTS;
        UOTGHS->UOTGHS_DEVEPT &= ~( UOTGHS_DEVEPT_EPRST1 );
        reset_data ();

        // Enable interrupt
        Bank_Out::UEP::enable_register () = UOTGHS_DEVEPTIER_RXOUTES;
    }
}

bool
Bank_Out::release ()
{
    if ( ( UEP::mask_register () & UOTGHS_DEVEPTIMR_FIFOCON ) != 0 ) {
        // Be sure this is not a new bank
        if ( ( UEP::status_register () & UOTGHS_DEVEPTISR_RXOUTI ) == 0 ) {
            // Clear FIFOCON bit
            UEP::disable_register () = UOTGHS_DEVEPTIDR_FIFOCONC;
            reset_data ();
            return true;
        }
    }
    return false;
}

void
Bank_Out::process_interrupt ()
{
    if ( ( UEP::status_register () & UOTGHS_DEVEPTISR_RXOUTI ) != 0 ) {
        UEP::clear_register () = UOTGHS_DEVEPTICR_RXOUTIC;
        std::uint32_t bank_size =
            ( UEP::status_register () & UOTGHS_DEVEPTISR_BYCT_Msk ) >>
            UOTGHS_DEVEPTISR_BYCT_Pos;
        _cur = UEP::bank ();
        _end = UEP::bank () + bank_size;
        _callback ();
    }
}

} // namespace adue::usb::bulk_io
