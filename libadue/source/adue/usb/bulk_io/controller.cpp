/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#include <adue/no_op.hpp>
#include <adue/pmc.hpp>
#include <adue/serialize.hpp>
#include <adue/usb/bulk_io/controller.hpp>

namespace adue::usb::bulk_io
{

static const std::uint8_t desc_str_idx_manufacturer = 1;
static const std::uint8_t desc_str_idx_product = 2;
static const std::uint8_t desc_str_idx_serial = 3;
static const std::uint8_t desc_str_idx_configuration = 4;
static const std::uint8_t desc_str_idx_interface = 5;

void
Controller::init ()
{
    {
        // Detach
        UOTGHS->UOTGHS_DEVCTRL |= UOTGHS_DEVCTRL_DETACH;

        // Disable USB controller
        UOTGHS->UOTGHS_CTRL &= ~UOTGHS_CTRL_USBE;
        // Freeze controller clock
        UOTGHS->UOTGHS_CTRL |= UOTGHS_CTRL_FRZCLK;

        // Disable USB-On-The-Go peripheral clock
        pmc_disable_periph_clk ( ID_UOTGHS );
    }

    _is_enabled = uint32_false;
    _enable_address = uint32_false;
    _configuration = 0;
    _interface = 0;

    _bank_control.reset_data ();
    _bank_out.reset_data ();
    _bank_out.set_callback ( &adue::no_op );
    _bank_in.reset_data ();
    _bank_in.set_callback ( &adue::no_op );

    _callback_eor = &adue::no_op;
    _callback_config = &adue::no_op;
    _callback_interface = &adue::no_op;
    _callback_sof = &adue::no_op;

    _setup.type = Setup_Type::NONE;
    _setup.write = Setup_Write::NONE;
    _setup.read = Setup_Read::NONE;
    // Clear string references
    for ( auto & item : _setup.desc.string_dref ) {
        item.clear ();
    }

    init_descriptors ();
}

void
Controller::init_descriptors ()
{
    // Device descriptor
    {
        _setup.desc.device.bLength = 18;
        _setup.desc.device.bDescriptorType = usb::DESC_TYPE_DEVICE;
        _setup.desc.device.bcdUSB_0 = 0x00;
        _setup.desc.device.bcdUSB_1 = 0x02;
        _setup.desc.device.bDeviceClass = 0x00;
        _setup.desc.device.bDeviceSubClass = 0x00;
        _setup.desc.device.bDeviceProtocol = 0x00;
        _setup.desc.device.bMaxPacketSize = Bank_Control::bank_size;
        adue::set_8le_uint16 ( &_setup.desc.device.idVendor_0, USB_ID_VENDOR );
        adue::set_8le_uint16 ( &_setup.desc.device.idProduct_0,
                               USB_ID_PRODUCT );
        _setup.desc.device.bcdDevice_0 = 0x00;
        _setup.desc.device.bcdDevice_1 = 0x01;
        _setup.desc.device.iManufacturer = 0;
        _setup.desc.device.iProduct = 0;
        _setup.desc.device.iSerialNumber = 0;
        _setup.desc.device.bNumConfigurations = 1;
    }
    // Configuration descriptor
    {
        _setup.desc.configuration.bLength =
            sizeof ( usb::Descriptor_Configuration );
        _setup.desc.configuration.bDescriptorType =
            usb::DESC_TYPE_CONFIGURATION;
        adue::set_8le_uint16 (
            &_setup.desc.configuration.wTotalLenght_0,
            sizeof ( usb::Descriptor_Configuration ) +
                sizeof ( usb::Descriptor_Interface ) +
                ( sizeof ( usb::Descriptor_Endpoint ) * 2 ) );
        _setup.desc.configuration.bNumInterfaces = 1;
        _setup.desc.configuration.bConfigurationValue = 1;
        _setup.desc.configuration.iConfiguration = 0;
        _setup.desc.configuration.bmAttributes =
            usb::DESC_ATTR_BIT_SELF_POWERED | usb::DESC_ATTR_BIT_BUS_POWERED;
        _setup.desc.configuration.bMaxPower = 250;
    }
    // Interface descriptor
    {
        _setup.desc.interface.bLength = sizeof ( usb::Descriptor_Interface );
        _setup.desc.interface.bDescriptorType = usb::DESC_TYPE_INTERFACE;
        _setup.desc.interface.bInterfaceNumber = 0;
        _setup.desc.interface.bAlternateSetting = 0;
        _setup.desc.interface.bNumEndpoints = 2;
        _setup.desc.interface.bInterfaceClass = 0xFF;
        _setup.desc.interface.bInterfaceSubClass = 0xFF;
        _setup.desc.interface.bInterfaceProtocol = 0xFF;
        _setup.desc.interface.iInterface = 0;
    }
    // Endpoint descriptor: Host OUT
    {
        _setup.desc.ept_out.bLength = sizeof ( usb::Descriptor_Endpoint );
        _setup.desc.ept_out.bDescriptorType = usb::DESC_TYPE_ENDPOINT;
        _setup.desc.ept_out.bEndpointAddress =
            ( 0x01 << usb::DESC_EPT_ADDR_NUMBER_POS ) |
            usb::DESC_EPT_ADDR_DIR_OUT;
        _setup.desc.ept_out.bmAttributes = usb::DESC_EPT_ATTR_TRANSFER_BULK;
        adue::set_8le_uint16 ( &_setup.desc.ept_out.wMaxPacketSize_0,
                               Bank_Out::bank_size );
        _setup.desc.ept_out.bInterval = 1;
    }
    // Endpoint descriptor: Host IN
    {
        _setup.desc.ept_in.bLength = sizeof ( usb::Descriptor_Endpoint );
        _setup.desc.ept_in.bDescriptorType = usb::DESC_TYPE_ENDPOINT;
        _setup.desc.ept_in.bEndpointAddress =
            ( 0x02 << usb::DESC_EPT_ADDR_NUMBER_POS ) |
            usb::DESC_EPT_ADDR_DIR_IN;
        _setup.desc.ept_in.bmAttributes = usb::DESC_EPT_ATTR_TRANSFER_BULK;
        adue::set_8le_uint16 ( &_setup.desc.ept_in.wMaxPacketSize_0,
                               Bank_In::bank_size );
        _setup.desc.ept_in.bInterval = 1;
    }

    // String descriptors
    _setup.desc.string_header.bLength = 2;
    _setup.desc.string_header.bDescriptorType = usb::DESC_TYPE_STRING;
    _setup.desc.string_langs[ 0 ] = 0x09; // wLANGID[0] low
    _setup.desc.string_langs[ 1 ] = 0x04; // wLANGID[0] high
}

void
Controller::init_desc_string ( Descriptor_String index_n,
                               const adue::Data_Ref & dref_n )
{
    if ( !is_enabled () && ( index_n < Descriptor_String::USER ) ) {
        adue::Data_Ref & string_dref (
            _setup.desc.string_dref[ (std::uint32_t)index_n ] );
        if ( string_dref != dref_n ) {
            string_dref = dref_n;
            {
                std::uint32_t dstring_index ( 0 );
                if ( string_dref.is_valid () ) {
                    dstring_index =
                        static_cast< std::uint32_t > ( index_n ) + 1;
                }
                switch ( index_n ) {
                case Descriptor_String::MANUFACTURER:
                    _setup.desc.device.iManufacturer = dstring_index;
                    break;
                case Descriptor_String::PRODUCT:
                    _setup.desc.device.iProduct = dstring_index;
                    break;
                case Descriptor_String::SERIAL:
                    _setup.desc.device.iSerialNumber = dstring_index;
                    break;
                case Descriptor_String::CONFIG:
                    _setup.desc.configuration.iConfiguration = dstring_index;
                    break;
                case Descriptor_String::INTERFACE:
                    _setup.desc.interface.iInterface = dstring_index;
                    break;
                default:
                    break;
                }
            }
        }
    }
}

void
Controller::disable ()
{
    if ( is_enabled () ) {
        // Detach device
        detach ();

        // Disable endpoints
        UOTGHS->UOTGHS_DEVEPT &= ~( UOTGHS_DEVEPT_EPEN0 | UOTGHS_DEVEPT_EPEN1 |
                                    UOTGHS_DEVEPT_EPEN2 );

        // Clear all endpoint configurations
        for ( unsigned int ii = 0; ii != 9; ++ii ) {
            UOTGHS->UOTGHS_DEVEPTCFG[ ii ] = 0;
        }

        // Disable USB-On-The-Go interrupts
        NVIC_DisableIRQ ( (IRQn_Type)ID_UOTGHS );
        // Disable interrupts
        UOTGHS->UOTGHS_DEVIDR = ( UOTGHS_DEVIDR_EORSTEC | UOTGHS_DEVIDR_SOFEC |
                                  UOTGHS_DEVIDR_PEP_0 );

        // Disable USB controller
        UOTGHS->UOTGHS_CTRL &= ~UOTGHS_CTRL_USBE;
        // Freeze controller clock
        UOTGHS->UOTGHS_CTRL |= UOTGHS_CTRL_FRZCLK;
        // Disable USB-On-The-Go peripheral clock
        pmc_disable_periph_clk ( ID_UOTGHS );

        _is_enabled = uint32_false;
        _enable_address = uint32_false;
        _configuration = 0;
        _interface = 0;
    }
}

void
Controller::enable ()
{
    if ( !is_enabled () ) {
        _is_enabled = uint32_true;

        // Enable USB-On-The-Go peripheral clock
        pmc_enable_periph_clk ( ID_UOTGHS );
        // Enable UPLL clock
        pmc_enable_upll_clock ();
        // Switch to use UPLL clock for USB
        pmc_switch_udpck_to_upllck ( 0 ); // div=0+1
        // Enable USB clock
        pmc_enable_udpck ();

        // Disable USB host/device mode auto selection
        UOTGHS->UOTGHS_CTRL &= ~UOTGHS_CTRL_UIDE;
        // Enable USB device mode
        UOTGHS->UOTGHS_CTRL |= UOTGHS_CTRL_UIMOD;

        // Reset USB-On-The-Go Pad
        UOTGHS->UOTGHS_CTRL &= ~UOTGHS_CTRL_OTGPADE;
        UOTGHS->UOTGHS_CTRL |= UOTGHS_CTRL_OTGPADE;

        // Disable forced low speed
        UOTGHS->UOTGHS_DEVCTRL &= ~UOTGHS_DEVCTRL_LS;
        // Select speed
        UOTGHS->UOTGHS_DEVCTRL =
            ( UOTGHS->UOTGHS_DEVCTRL & UOTGHS_DEVCTRL_SPDCONF_Msk ) |
            UOTGHS_DEVCTRL_SPDCONF_NORMAL;

        // Enable USB-On-The-Go
        UOTGHS->UOTGHS_CTRL |= UOTGHS_CTRL_USBE;
        // Unfreeze controller clock
        UOTGHS->UOTGHS_CTRL &= ~UOTGHS_CTRL_FRZCLK;
        // Wait until clock is usable
        while ( ( UOTGHS->UOTGHS_SR & UOTGHS_SR_CLKUSABLE ) == 0 ) {
        };

        // Enable interrupt on "End of reset" and "Start of frame"
        UOTGHS->UOTGHS_DEVIER = UOTGHS_DEVIER_EORSTES | UOTGHS_DEVIER_SOFES;
    }
}

void
Controller::attach ()
{
    if ( is_enabled () ) {
        // Attach device
        UOTGHS->UOTGHS_DEVCTRL &= ~UOTGHS_DEVCTRL_DETACH;
    }
}

void
Controller::detach ()
{
    // Detach device
    UOTGHS->UOTGHS_DEVCTRL |= UOTGHS_DEVCTRL_DETACH;
}

void
Controller::setup_endpoints ()
{
    // Clear all endpoint configurations
    for ( unsigned int ii = 0; ii != 9; ++ii ) {
        UOTGHS->UOTGHS_DEVEPTCFG[ ii ] = 0;
    }

    // Setup endpoint 0 (control)
    {
        // Enable endpoint 0
        UOTGHS->UOTGHS_DEVEPT |= UOTGHS_DEVEPT_EPEN0;
        // Configure endpoint 0
        Bank_Control::UEP::config_register () |= UOTGHS_DEVEPTCFG_EPTYPE_CTRL;
        Bank_Control::UEP::config_register () |=
            UOTGHS_DEVEPTCFG_EPSIZE_64_BYTE;
        Bank_Control::UEP::config_register () |= UOTGHS_DEVEPTCFG_EPBK_1_BANK;
        Bank_Control::UEP::config_register () |= UOTGHS_DEVEPTCFG_ALLOC;
        while ( ( Bank_Control::UEP::status_register () &
                  UOTGHS_DEVEPTISR_CFGOK ) == 0 ) {
        };

        // Enable endpoint 0 interrupt for setup packets
        Bank_Control::UEP::enable_register () = UOTGHS_DEVEPTIER_RXSTPES;
        // Enable UOTGHS interrupt on EP0 interrupt
        UOTGHS->UOTGHS_DEVIER = UOTGHS_DEVIER_PEP_0;
    }
    // Setup endpoint 1 (OUT)
    {
        // Enable endpoint 1
        UOTGHS->UOTGHS_DEVEPT |= UOTGHS_DEVEPT_EPEN1;
        // Configure endpoint 1
        Bank_Out::UEP::config_register () |= UOTGHS_DEVEPTCFG_EPTYPE_BLK;
        Bank_Out::UEP::config_register () |= UOTGHS_DEVEPTCFG_EPDIR_OUT;
        Bank_Out::UEP::config_register () |= UOTGHS_DEVEPTCFG_EPSIZE_512_BYTE;
        Bank_Out::UEP::config_register () |= UOTGHS_DEVEPTCFG_EPBK_3_BANK;
        Bank_Out::UEP::config_register () |= UOTGHS_DEVEPTCFG_ALLOC;
        while ( ( Bank_Out::UEP::status_register () &
                  UOTGHS_DEVEPTISR_CFGOK ) == 0 ) {
        };

        // Enable UOTGHS interrupt on EP1 interrupt
        UOTGHS->UOTGHS_DEVIER = UOTGHS_DEVIER_PEP_1;
    }
    // Setup endpoint 2 (IN)
    {
        // Enable endpoint 2
        UOTGHS->UOTGHS_DEVEPT |= UOTGHS_DEVEPT_EPEN2;
        // Configure endpoint 2
        Bank_In::UEP::config_register () |= UOTGHS_DEVEPTCFG_EPTYPE_BLK;
        Bank_In::UEP::config_register () |= UOTGHS_DEVEPTCFG_EPDIR_IN;
        Bank_In::UEP::config_register () |= UOTGHS_DEVEPTCFG_EPSIZE_512_BYTE;
        Bank_In::UEP::config_register () |= UOTGHS_DEVEPTCFG_EPBK_3_BANK;
        Bank_In::UEP::config_register () |= UOTGHS_DEVEPTCFG_ALLOC;
        while ( ( Bank_In::UEP::status_register () & UOTGHS_DEVEPTISR_CFGOK ) ==
                0 ) {
        };

        // Enable UOTGHS interrupt on EP2 interrupt
        UOTGHS->UOTGHS_DEVIER = UOTGHS_DEVIER_PEP_2;
    }
}

void
Controller::process_end_of_reset ()
{
    // Reset address to 0
    UOTGHS->UOTGHS_DEVCTRL &= ~UOTGHS_DEVCTRL_UADD_Msk;
    // Enable address 0
    UOTGHS->UOTGHS_DEVCTRL |= UOTGHS_DEVCTRL_ADDEN;

    // Clear selected configuration
    _enable_address = uint32_false;
    _configuration = 1;
    _interface = 0;

    // Setup endpoints
    setup_endpoints ();
    reset_data_endpoints ();

    // Acknowledge end of reset interrupt
    UOTGHS->UOTGHS_DEVICR = UOTGHS_DEVICR_EORSTC;

    // Notify callback
    _callback_eor ();
}

void
Controller::process_control ()
{
    // --- Check if there is a new setup package
    //
    if ( ( Bank_Control::UEP::status_register () & UOTGHS_DEVEPTISR_RXSTPI ) !=
         0 ) {
        // Reset bank data pointer
        bank_control ().reset_data ();
        // Compile time assert size of setup package
        static_assert ( sizeof ( usb::Setup_Package ) == 8,
                        "size of setup package must be 8" );
        // Read setup package from bank
        bank_control ().read (
            reinterpret_cast< std::uint8_t * > ( &_setup.packet ),
            sizeof ( usb::Setup_Package ) );
        // Acknowledge setup package
        Bank_Control::UEP::clear_register () = UOTGHS_DEVEPTICR_RXSTPIC;

        // Analyse setup packet request direction
        if ( ( _setup.packet.bmRequestType & usb::SETUP_REQ_BITS_DIR ) ==
             usb::SETUP_REQ_DIR_HOST_TO_DEVICE ) {
            // --- Control write request
            // Host OUT / host to device
            _setup.type = Setup_Type::WRITE;
            _setup.write = Setup_Write::BEGIN;
        } else {
            // --- Control read request
            // Host IN / device to host /
            _setup.type = Setup_Type::READ;
            _setup.read = Setup_Read::BEGIN;
        }
        _setup.bad = false;
    }

    // --- Continue setup package processing
    //
    if ( _setup.type != Setup_Type::NONE ) {

        // --- Control write (OUT) transfer
        //
        if ( _setup.type == Setup_Type::WRITE ) {
            if ( _setup.write == Setup_Write::BEGIN ) {
                // Enable setup interrupts
                Bank_Control::UEP::enable_register () =
                    ( UOTGHS_DEVEPTIER_RXOUTES | UOTGHS_DEVEPTIER_TXINES |
                      UOTGHS_DEVEPTIER_NAKINES );

                // First process
                if ( process_control_write_begin () ) {
                    _setup.write = Setup_Write::COMPLETE;
                } else {
                    _setup.write = Setup_Write::READ_MORE;
                }
            }
            {
                // Check if transfer was finished by the host
                if ( ( Bank_Control::UEP::status_register () &
                       UOTGHS_DEVEPTISR_NAKINI ) != 0 ) {
                    _setup.write = Setup_Write::COMPLETE;
                }
                if ( ( Bank_Control::UEP::status_register () &
                       UOTGHS_DEVEPTISR_TXINI ) != 0 ) {
                    // Check if we feel complete
                    if ( _setup.write == Setup_Write::COMPLETE ) {
                        // Check id the setup request was good
                        if ( !_setup.bad ) {
                            // Finish with a ZLP (zero length packet)
                            Bank_Control::UEP::clear_register () =
                                UOTGHS_DEVEPTICR_TXINIC;
                            _bank_control.reset_data ();
                        } else {
                            // Bad setup request. STALL
                            Bank_Control::UEP::enable_register () =
                                UOTGHS_DEVEPTIER_STALLRQS;
                            _bank_control.reset_data ();
                        }
                        // Setup request processed
                        _setup.write = Setup_Write::NONE;
                    }
                }
                // Read more data on demand
                if ( ( Bank_Control::UEP::status_register () &
                       UOTGHS_DEVEPTISR_RXOUTI ) != 0 ) {
                    // Data bank is readable
                    process_control_write_read_more ();
                    // Acknowledge data and flush bank
                    Bank_Control::UEP::clear_register () =
                        UOTGHS_DEVEPTICR_RXOUTIC;
                    _bank_control.reset_data ();
                }
            }
            if ( _setup.write == Setup_Write::NONE ) {
                if ( _enable_address != uint32_false ) {
                    _enable_address = uint32_false;
                    // Wait until send ready before enabling the address (!)
                    while ( ( Bank_Control::UEP::status_register () &
                              UOTGHS_DEVEPTISR_TXINI ) == 0 ) {
                    };
                    // Enable address
                    UOTGHS->UOTGHS_DEVCTRL |= UOTGHS_DEVCTRL_ADDEN;
                }
                // Disable setup interrupts
                Bank_Control::UEP::disable_register () =
                    ( UOTGHS_DEVEPTIDR_RXOUTEC | UOTGHS_DEVEPTIDR_TXINEC |
                      UOTGHS_DEVEPTIDR_NAKINEC );
                // Clear setup type
                _setup.type = Setup_Type::NONE;
            }
        }

        // --- Control read (IN) transfer
        //
        if ( _setup.type == Setup_Type::READ ) {
            if ( _setup.read == Setup_Read::BEGIN ) {
                // Enable setup interrupts
                Bank_Control::UEP::enable_register () =
                    ( UOTGHS_DEVEPTIER_RXOUTES | UOTGHS_DEVEPTIER_TXINES );

                process_control_read_begin ();
                _setup.read = Setup_Read::WRITE_MORE;
            }
            {
                // Check if the transfer is complete
                if ( ( Bank_Control::UEP::status_register () &
                       UOTGHS_DEVEPTISR_RXOUTI ) != 0 ) {
                    // Read transfer ended.
                    // Acknowledge by clearing RXOUTI.
                    Bank_Control::UEP::clear_register () =
                        UOTGHS_DEVEPTICR_RXOUTIC;
                    _bank_control.reset_data ();
                    // Setup request processed
                    _setup.read = Setup_Read::NONE;
                } else if ( ( Bank_Control::UEP::status_register () &
                              UOTGHS_DEVEPTISR_TXINI ) != 0 ) {
                    // Data bank is writable.
                    if ( _setup.read == Setup_Read::WRITE_MORE ) {
                        if ( process_control_read_send_more () ) {
                            // Don't read more but wait for RXOUTI
                            _setup.read = Setup_Read::DONE;
                        }
                    }
                    if ( !_setup.bad ) {
                        // Flush data bank
                        Bank_Control::UEP::clear_register () =
                            UOTGHS_DEVEPTICR_TXINIC;
                        _bank_control.reset_data ();
                    } else {
                        // Bad setup request. STALL
                        Bank_Control::UEP::enable_register () =
                            UOTGHS_DEVEPTIER_STALLRQS;
                        _bank_control.reset_data ();
                        // Setup request processed
                        _setup.read = Setup_Read::NONE;
                    }
                }
            }
            // Check if this transfer is done
            if ( _setup.read == Setup_Read::NONE ) {
                // Disable setup interrupts
                Bank_Control::UEP::disable_register () =
                    ( UOTGHS_DEVEPTIDR_RXOUTEC | UOTGHS_DEVEPTIDR_TXINEC );
                // Clear setup type
                _setup.type = Setup_Type::NONE;
            }
        }
    }
}

bool
Controller::process_control_write_begin ()
{
    bool transfer_done ( true );

    const std::uint8_t req_recipient ( _setup.packet.bmRequestType &
                                       usb::SETUP_REQ_BITS_RECIPIENT );
    const std::uint8_t req_type ( _setup.packet.bmRequestType &
                                  usb::SETUP_REQ_BITS_TYPE );
    const std::uint8_t req ( _setup.packet.bRequest );

    switch ( req_recipient ) {
    case usb::SETUP_REQ_RECIPIENT_DEVICE:
        switch ( req_type ) {
        case usb::SETUP_REQ_TYPE_STANDARD:
            switch ( req ) {
            case usb::SETUP_CLEAR_FEATURE:
            case usb::SETUP_SET_FEATURE:
                // Not supported
                _setup.bad = true;
                break;
            case usb::SETUP_SET_ADDRESS:
                // Load address
                {
                    std::uint32_t addr_bits ( _setup.packet.wValue_0 );
                    addr_bits = ( addr_bits << UOTGHS_DEVCTRL_UADD_Pos );
                    addr_bits &= UOTGHS_DEVCTRL_UADD_Msk;
                    UOTGHS->UOTGHS_DEVCTRL =
                        ( UOTGHS->UOTGHS_DEVCTRL & UOTGHS_DEVCTRL_UADD_Msk ) |
                        addr_bits;
                }
                // Keep address disabled until ZLP has been sent
                UOTGHS->UOTGHS_DEVCTRL &= ~UOTGHS_DEVCTRL_ADDEN;
                _enable_address = uint32_true;
                break;
            case usb::SETUP_SET_DESCRIPTOR:
                // Not supported
                _setup.bad = true;
                break;
            case usb::SETUP_SET_CONFIGURATION:
                // Select configuration - we only have one
                if ( _setup.packet.wValue_0 == 1 ) {
                    // Select configuration
                    _configuration = _setup.packet.wValue_0;
                    _interface = 0;
                    // Reset
                    reset_data_endpoints ();
                    // Notify callback
                    _callback_config ();
                } else {
                    // Invalid configuration
                    _setup.bad = true;
                }
                break;
            default:
                _setup.bad = true;
                break;
            }
            break;
        case usb::SETUP_REQ_TYPE_CLASS:
        case usb::SETUP_REQ_TYPE_VENDOR:
        case usb::SETUP_REQ_TYPE_RESERVED:
            _setup.bad = true;
            break;
        default:
            _setup.bad = true;
            break;
        }
        break;

    case usb::SETUP_REQ_RECIPIENT_INTERFACE:
        switch ( req_type ) {
        case usb::SETUP_REQ_TYPE_STANDARD:
            switch ( req ) {
            case usb::SETUP_CLEAR_FEATURE:
            case usb::SETUP_SET_FEATURE:
                _setup.bad = true;
                break;
            case usb::SETUP_SET_INTERFACE:
                // Select an alternate interface - we only have one
                if ( _configuration == 1 ) {
                    if ( _setup.packet.wValue_0 == 0 ) {
                        _interface = _setup.packet.wValue_0;
                        // Reset
                        reset_data_endpoints ();
                        // Notify callback
                        _callback_interface ();
                    } else {
                        // Invalid interface
                        _setup.bad = true;
                    }
                } else {
                    // Not configured, yet
                    _setup.bad = true;
                }
                break;
            default:
                _setup.bad = true;
                break;
            }
            break;
        case usb::SETUP_REQ_TYPE_CLASS:
        case usb::SETUP_REQ_TYPE_VENDOR:
        case usb::SETUP_REQ_TYPE_RESERVED:
            _setup.bad = true;
            break;
        default:
            _setup.bad = true;
            break;
        }
        break;

    case usb::SETUP_REQ_RECIPIENT_ENDPOINT:
        switch ( req_type ) {
        case usb::SETUP_REQ_TYPE_STANDARD:
            switch ( req ) {
            case usb::SETUP_CLEAR_FEATURE:
            case usb::SETUP_SET_FEATURE:
                _setup.bad = true;
                break;
            default:
                _setup.bad = true;
                break;
            }
            break;
        case usb::SETUP_REQ_TYPE_CLASS:
        case usb::SETUP_REQ_TYPE_VENDOR:
        case usb::SETUP_REQ_TYPE_RESERVED:
            _setup.bad = true;
            break;
        default:
            _setup.bad = true;
            break;
        }
        break;

    case usb::SETUP_REQ_RECIPIENT_OTHER:
        switch ( req_type ) {
        case usb::SETUP_REQ_TYPE_STANDARD:
        case usb::SETUP_REQ_TYPE_CLASS:
        case usb::SETUP_REQ_TYPE_VENDOR:
        case usb::SETUP_REQ_TYPE_RESERVED:
            _setup.bad = true;
            break;
        default:
            _setup.bad = true;
            break;
        }
        break;

    default:
        _setup.bad = true;
        break;
    }

    return transfer_done;
}

void
Controller::process_control_write_read_more ()
{
}

void
Controller::process_control_read_begin ()
{
    const std::uint8_t req_recipient ( _setup.packet.bmRequestType &
                                       usb::SETUP_REQ_BITS_RECIPIENT );
    const std::uint8_t req_type ( _setup.packet.bmRequestType &
                                  usb::SETUP_REQ_BITS_TYPE );
    const std::uint8_t req ( _setup.packet.bRequest );

    if ( req_recipient == usb::SETUP_REQ_RECIPIENT_DEVICE ) {
        if ( req_type == usb::SETUP_REQ_TYPE_STANDARD ) {
            if ( req == usb::SETUP_GET_DESCRIPTOR ) {
                // Descriptor requested
                process_control_read_begin_descriptor ();
            }
        }
    }
}

void
Controller::process_control_read_begin_descriptor ()
{
    _setup.desc_send.num_sendable =
        adue::read_uint16_8le ( &_setup.packet.wLength_0 );
    _setup.desc_send.cur_pos = 0;
    _setup.desc_send.cur = &_setup.desc_send.dref[ 0 ];
    _setup.desc_send.end = _setup.desc_send.cur;

    const std::uint8_t desc_type ( _setup.packet.wValue_1 );
    if ( desc_type == usb::DESC_TYPE_DEVICE ) {
        // --- Device descriptor
        //
        // Setup data references
        _setup.desc_send.dref[ 0 ].set (
            (const std::uint8_t *)&_setup.desc.device,
            _setup.desc.device.bLength );
        // Update end iterator
        _setup.desc_send.end += 1;
    } else if ( desc_type == usb::DESC_TYPE_CONFIGURATION ) {
        // --- Configuration descriptor
        //
        // Setup data references
        _setup.desc_send.dref[ 0 ].set (
            (const std::uint8_t *)( &_setup.desc.configuration ),
            _setup.desc.configuration.bLength );
        _setup.desc_send.dref[ 1 ].set (
            (const std::uint8_t *)( &_setup.desc.interface ),
            _setup.desc.interface.bLength );
        _setup.desc_send.dref[ 2 ].set (
            (const std::uint8_t *)( &_setup.desc.ept_out ),
            _setup.desc.ept_out.bLength );
        _setup.desc_send.dref[ 3 ].set (
            (const std::uint8_t *)( &_setup.desc.ept_in ),
            _setup.desc.ept_in.bLength );
        // Update end iterator
        _setup.desc_send.end += 4;
    } else if ( desc_type == usb::DESC_TYPE_STRING ) {
        // --- String descriptor
        //
        const std::uint32_t desc_header_size (
            sizeof ( usb::Descriptor_String_Header ) );
        if ( _setup.packet.wValue_0 == 0 ) {
            // -- Available languages descriptor
            //
            // Adjust descriptor length
            _setup.desc.string_header.bLength =
                desc_header_size + sizeof ( _setup.desc.string_langs );
            // Setup data references
            _setup.desc_send.dref[ 0 ].set (
                (const std::uint8_t *)( &_setup.desc.string_header ),
                desc_header_size );
            _setup.desc_send.dref[ 1 ].set (
                &_setup.desc.string_langs[ 0 ],
                sizeof ( _setup.desc.string_langs ) );
            // Update end iterator
            _setup.desc_send.end += 2;
        } else {
            // -- Actual string descriptor
            //
            // Assume empty string
            _setup.desc_send.dref[ 0 ].set (
                (const std::uint8_t *)( &_setup.desc.string_header ),
                desc_header_size );
            _setup.desc.string_header.bLength = desc_header_size;
            // Update end iterator
            ++_setup.desc_send.end;

            // Try to find the requested string
            adue::Data_Ref & string_dref ( _setup.desc_send.dref[ 1 ] );
            string_dref.clear ();
            if ( ( _setup.packet.wIndex_0 == _setup.desc.string_langs[ 0 ] ) &&
                 ( _setup.packet.wIndex_1 == _setup.desc.string_langs[ 1 ] ) ) {
                // Default language requested
                Descriptor_String desc_str = Descriptor_String::USER;
                switch ( _setup.packet.wValue_0 ) {
                case desc_str_idx_manufacturer:
                    desc_str = Descriptor_String::MANUFACTURER;
                    break;
                case desc_str_idx_product:
                    desc_str = Descriptor_String::PRODUCT;
                    break;
                case desc_str_idx_serial:
                    desc_str = Descriptor_String::SERIAL;
                    break;
                case desc_str_idx_configuration:
                    desc_str = Descriptor_String::CONFIG;
                    break;
                case desc_str_idx_interface:
                    desc_str = Descriptor_String::INTERFACE;
                    break;
                default:
                    break;
                }
                if ( desc_str != Descriptor_String::USER ) {
                    string_dref =
                        _setup.desc.string_dref[ static_cast< std::uint32_t > (
                            desc_str ) ];
                }
            }
            // If the string data is valid append it to the send list
            if ( string_dref.is_valid () ) {
                std::uint32_t size_extra ( string_dref.size () );
                const std::uint32_t size_extra_max ( 255 - desc_header_size );
                if ( size_extra > size_extra_max ) {
                    size_extra = size_extra_max;
                }
                // Update descriptor length
                _setup.desc.string_header.bLength +=
                    std::uint8_t ( size_extra );
                // Update end iterator
                ++_setup.desc_send.end;
            } else {
                --_setup.desc_send.end;
                _setup.bad = true;
            }
        }
    } else {
        _setup.bad = true;
    }
}

bool
Controller::process_control_read_send_more ()
{
    bool transfer_complete ( true );

    const std::uint8_t req_recipient ( _setup.packet.bmRequestType &
                                       usb::SETUP_REQ_BITS_RECIPIENT );
    const std::uint8_t req_type ( _setup.packet.bmRequestType &
                                  usb::SETUP_REQ_BITS_TYPE );
    const std::uint8_t req ( _setup.packet.bRequest );
    switch ( req_recipient ) {

    case usb::SETUP_REQ_RECIPIENT_DEVICE:

        switch ( req_type ) {
        case usb::SETUP_REQ_TYPE_STANDARD:
            switch ( req ) {
            case usb::SETUP_GET_STATUS:
                // Not self powered, not sending wake up requests
                bank_control ().write_byte ( 0 );
                bank_control ().write_byte ( 0 );
                break;
            case usb::SETUP_GET_DESCRIPTOR:
                // Descriptor writing may span over multiple bank writes
                transfer_complete =
                    process_control_read_send_more_descriptor ();
                break;
            case usb::SETUP_GET_CONFIGURATION:
                // Send current configuration
                bank_control ().write_byte ( _configuration );
                break;
            default:
                _setup.bad = true;
                break;
            }
            break;
        case usb::SETUP_REQ_TYPE_CLASS:
        case usb::SETUP_REQ_TYPE_VENDOR:
        case usb::SETUP_REQ_TYPE_RESERVED:
            _setup.bad = true;
            break;
        default:
            _setup.bad = true;
            break;
        }
        break;

    case usb::SETUP_REQ_RECIPIENT_INTERFACE:

        switch ( req_type ) {
        case usb::SETUP_REQ_TYPE_STANDARD:
            switch ( req ) {
            case usb::SETUP_GET_STATUS:
                // Return two bytes
                bank_control ().write_byte ( 0 );
                bank_control ().write_byte ( 0 );
                break;
            case usb::SETUP_GET_INTERFACE:
                // Send current interface
                bank_control ().write_byte ( _interface );
                break;
            default:
                _setup.bad = true;
                break;
            }
            break;
        case usb::SETUP_REQ_TYPE_CLASS:
        case usb::SETUP_REQ_TYPE_VENDOR:
        case usb::SETUP_REQ_TYPE_RESERVED:
            _setup.bad = true;
            break;
        default:
            _setup.bad = true;
            break;
        }
        break;

    case usb::SETUP_REQ_RECIPIENT_ENDPOINT:

        switch ( req_type ) {
        case usb::SETUP_REQ_TYPE_STANDARD:
            switch ( req ) {
            case usb::SETUP_GET_STATUS:
            case usb::SETUP_SYNCH_FRAME:
                _setup.bad = true;
                break;
            default:
                _setup.bad = true;
                break;
            }
            break;
        case usb::SETUP_REQ_TYPE_CLASS:
        case usb::SETUP_REQ_TYPE_VENDOR:
        case usb::SETUP_REQ_TYPE_RESERVED:
            _setup.bad = true;
            break;
        default:
            _setup.bad = true;
            break;
        }
        break;

    case usb::SETUP_REQ_RECIPIENT_OTHER:

        switch ( req_type ) {
        case usb::SETUP_REQ_TYPE_STANDARD:
        case usb::SETUP_REQ_TYPE_CLASS:
        case usb::SETUP_REQ_TYPE_VENDOR:
        case usb::SETUP_REQ_TYPE_RESERVED:
            _setup.bad = true;
            break;
        default:
            _setup.bad = true;
            break;
        }
        break;

    default:
        _setup.bad = true;
        break;
    }

    return transfer_complete;
}

bool
Controller::process_control_read_send_more_descriptor ()
{
    // Write data from the data references
    while ( _setup.desc_send.cur != _setup.desc_send.end ) {
        std::uint32_t num_send ( _setup.desc_send.cur->size () -
                                 _setup.desc_send.cur_pos );
        if ( num_send == 0 ) {
            // Go to next tile on demand
            _setup.desc_send.cur_pos = 0;
            ++_setup.desc_send.cur;
        } else {
            if ( num_send > _setup.desc_send.num_sendable ) {
                num_send = _setup.desc_send.num_sendable;
            }
            {
                const std::uint32_t num_max (
                    bank_control ().bytes_writable () );
                if ( num_send > num_max ) {
                    num_send = num_max;
                }
            }
            if ( num_send != 0 ) {
                // Write to bank
                bank_control ().write ( ( _setup.desc_send.cur->data () +
                                          _setup.desc_send.cur_pos ),
                                        num_send );
                _setup.desc_send.num_sendable -= num_send;
                _setup.desc_send.cur_pos += num_send;
            } else {
                break;
            }
        }
    }

    return ( _setup.desc_send.cur == _setup.desc_send.end );
}

void
Controller::reset_data_endpoints ()
{
    _bank_in.reset_endpoint ();
    _bank_out.reset_endpoint ();
}

} // namespace adue::usb::bulk_io
