/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#include <adue/usb/bulk_io/bank_in.hpp>

namespace adue::usb::bulk_io
{

void
Bank_In::reset_endpoint ()
{
    if ( ( UOTGHS->UOTGHS_DEVEPT & UOTGHS_DEVEPT_EPEN2 ) != 0 ) {
        // Disable interrupt
        Bank_In::UEP::disable_register () = UOTGHS_DEVEPTIDR_TXINEC;

        // Clear banks
        while ( ( Bank_In::UEP::status_register () &=
                  UOTGHS_DEVEPTISR_NBUSYBK_Msk ) != 0 ) {
            // Set kill bank flag and wait for it to be cleared
            Bank_In::UEP::enable_register () = UOTGHS_DEVEPTIER_KILLBKS;
            while ( ( Bank_In::UEP::mask_register () &=
                      UOTGHS_DEVEPTIMR_KILLBK ) != 0 ) {
            }
        }

        // Reset
        UOTGHS->UOTGHS_DEVEPT |= UOTGHS_DEVEPT_EPRST2;
        Bank_In::UEP::enable_register () = UOTGHS_DEVEPTIER_RSTDTS;
        UOTGHS->UOTGHS_DEVEPT &= ~( UOTGHS_DEVEPT_EPRST2 );
        reset_data ();

        // Enable interrupt
        Bank_In::UEP::enable_register () = UOTGHS_DEVEPTIER_TXINES;
    }
}

bool
Bank_In::submit ()
{
    if ( ( UEP::mask_register () & UOTGHS_DEVEPTIMR_FIFOCON ) != 0 ) {
        // Be sure this is not a new bank
        if ( ( UEP::status_register () & UOTGHS_DEVEPTISR_TXINI ) == 0 ) {
            // Clear FIFOCON bit
            UEP::disable_register () = UOTGHS_DEVEPTIDR_FIFOCONC;
            reset_data ();
            return true;
        }
    }
    return false;
}

void
Bank_In::process_interrupt ()
{
    if ( ( UEP::status_register () & UOTGHS_DEVEPTISR_TXINI ) != 0 ) {
        UEP::clear_register () = UOTGHS_DEVEPTICR_TXINIC;
        _cur = UEP::bank ();
        _end = UEP::bank () + bank_size;
        _callback ();
    }
}

} // namespace adue::usb::bulk_io
