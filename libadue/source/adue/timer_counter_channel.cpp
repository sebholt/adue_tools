/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#include <adue/timer_counter_channel.hpp>

namespace adue
{

void
Timer_Counter_Channel::select_meta_channel ( std::uint32_t channel_index_n )
{
    select_timer_channel ( ( channel_index_n / channels_per_timer ),
                           ( channel_index_n % channels_per_timer ) );
}

void
Timer_Counter_Channel::select_timer_channel ( std::uint32_t timer_index_n,
                                              std::uint32_t channel_index_n )
{
    // Sanitize  indices
    const std::uint32_t ti_max ( num_timers - 1 );
    const std::uint32_t ci_max ( channels_per_timer - 1 );
    if ( timer_index_n > ti_max ) {
        timer_index_n = ti_max;
        channel_index_n = ci_max;
    }
    if ( channel_index_n > ci_max ) {
        channel_index_n = ci_max;
    }

    switch ( timer_index_n ) {
    case 0:
        _timer_counter = TC0;
        break;
    case 1:
        _timer_counter = TC1;
        break;
    case 2:
        _timer_counter = TC2;
        break;
    default:
        break;
    }
    _channel = &( _timer_counter->TC_CHANNEL[ channel_index_n ] );
}

std::uint32_t
Timer_Counter_Channel::acquire_timer_index () const
{
    std::uint32_t res ( 3 );
    if ( _timer_counter == TC0 ) {
        res = 0;
    } else if ( _timer_counter == TC1 ) {
        res = 1;
    } else if ( _timer_counter == TC2 ) {
        res = 2;
    }
    return res;
}

std::uint32_t
Timer_Counter_Channel::acquire_channel_index () const
{
    return ( _channel - &( _timer_counter->TC_CHANNEL[ 0 ] ) );
}

std::uint32_t
Timer_Counter_Channel::acquire_meta_channel_index () const
{
    std::uint32_t res ( acquire_timer_index () );
    res *= channels_per_timer;
    res += acquire_channel_index ();
    return res;
}

void
Timer_Counter_Channel::setup_mode ( std::uint32_t mode_n,
                                    bool enable_interrupt_n )
{
    // Acquire IRQn_Type
    IRQn_Type irqn_type ( TC8_IRQn );
    {
        bool error ( false );
        switch ( acquire_meta_channel_index () ) {
        case 0:
            irqn_type = TC0_IRQn;
            break;
        case 1:
            irqn_type = TC1_IRQn;
            break;
        case 2:
            irqn_type = TC2_IRQn;
            break;
        case 3:
            irqn_type = TC3_IRQn;
            break;
        case 4:
            irqn_type = TC4_IRQn;
            break;
        case 5:
            irqn_type = TC5_IRQn;
            break;
        case 6:
            irqn_type = TC6_IRQn;
            break;
        case 7:
            irqn_type = TC7_IRQn;
            break;
        case 8:
            irqn_type = TC8_IRQn;
            break;
        default:
            error = true;
            break;
        }
        if ( error ) {
            return;
        }
    }
    // Disable interrupt
    NVIC_DisableIRQ ( irqn_type );

    pmc_set_writeprotect ( false );
    pmc_enable_periph_clk ( (std::uint32_t)irqn_type );

    {
        // Disable interrupts
        interrupts_disable ();
        // Disable clock
        disable ();
        // Clear interrupt status register
        clear_status ();

        // Set mode
        _channel->TC_CMR = mode_n;
    }

    if ( enable_interrupt_n ) {
        // Enable interrupt controller interrupts
        NVIC_EnableIRQ ( irqn_type );
    }
}

} // namespace adue
