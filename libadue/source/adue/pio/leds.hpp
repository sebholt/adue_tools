/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#pragma once

#include <adue/pio/pin.hpp>

namespace adue::pio
{

Pio * const led_p13_pio = PIOB;
constexpr std::uint32_t const led_p13_mask = 1u << 27;
using PIN_P13 = Pin_Output_T< PioB, led_p13_mask >;

Pio * const led_rx_pio = PIOC;
constexpr std::uint32_t const led_rx_mask = 1u << 30;
using PIN_RX = Pin_Output_T< PioC, led_rx_mask >;

Pio * const led_tx_pio = PIOA;
constexpr std::uint32_t const led_tx_mask = 1u << 21;
using PIN_TX = Pin_Output_T< PioA, led_tx_mask >;

/// - Enables the peripheral PIO clock
/// - Sets up the led pin as outputs
void
init_led_p13 ();

/// - Enables the peripheral PIO clock
/// - Sets up the led pin as outputs
void
init_led_rx ();

/// - Enables the peripheral PIO clock
/// - Sets up the led pin as outputs
void
init_led_tx ();

/// - Enables the peripheral PIO clocks
/// - Sets up the led pins as outputs
void
init_leds ();

inline void
set_led_p13 ( bool flag_n )
{
    pin_set ( led_p13_pio, led_p13_mask, flag_n );
}

inline bool
get_led_p13 ()
{
    return pin_out_is_high ( led_p13_pio, led_p13_mask );
}

inline void
toggle_led_p13 ()
{
    pin_toggle ( led_p13_pio, led_p13_mask );
}

inline void
set_led_rx ( bool flag_n )
{
    // The RX LED is inverted!
    pin_set ( led_rx_pio, led_rx_mask, !flag_n );
}

inline bool
get_led_rx ()
{
    // The RX LED is inverted!
    return !pin_out_is_high ( led_rx_pio, led_rx_mask );
}

inline void
toggle_led_rx ()
{
    pin_toggle ( led_rx_pio, led_rx_mask );
}

inline void
set_led_tx ( bool flag_n )
{
    // The TX LED is inverted!
    pin_set ( led_tx_pio, led_tx_mask, !flag_n );
}

inline bool
get_led_tx ()
{
    // The TX LED is inverted!
    return !pin_out_is_high ( led_tx_pio, led_tx_mask );
}

inline void
toggle_led_tx ()
{
    pin_toggle ( led_tx_pio, led_tx_mask );
}

} // namespace adue::pio
