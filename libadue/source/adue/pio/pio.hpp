/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#pragma once

#include <cstdint>
#include <sam3x8e.h>

namespace adue::pio
{

// -- Pin initialization

void
setup_as_output ( Pio volatile * port_n, std::uint32_t mask_n );

void
setup_as_output_peripheral_a ( Pio volatile * port_n, std::uint32_t mask_n );

void
setup_as_output_peripheral_b ( Pio volatile * port_n, std::uint32_t mask_n );

void
setup_as_input ( Pio volatile * port_n, std::uint32_t mask_n );

// -- Pin as output

inline bool
pin_out_is_high ( Pio volatile * pio_n, std::uint32_t pin_mask_n )
{
    return ( ( pio_n->PIO_ODSR & pin_mask_n ) != 0 );
}

inline void
pin_set_high ( Pio volatile * pio_n, std::uint32_t pin_mask_n )
{
    pio_n->PIO_SODR = pin_mask_n;
}

inline void
pin_set_low ( Pio volatile * pio_n, std::uint32_t pin_mask_n )
{
    pio_n->PIO_CODR = pin_mask_n;
}

inline void
pin_set ( Pio volatile * pio_n, std::uint32_t pin_mask_n, bool flag_n )
{
    if ( flag_n ) {
        pin_set_high ( pio_n, pin_mask_n );
    } else {
        pin_set_low ( pio_n, pin_mask_n );
    }
}

inline void
pin_toggle ( Pio volatile * pio_n, std::uint32_t pin_mask_n )
{
    if ( pin_out_is_high ( pio_n, pin_mask_n ) ) {
        pin_set_low ( pio_n, pin_mask_n );
    } else {
        pin_set_high ( pio_n, pin_mask_n );
    }
}

// -- Pin as input

inline bool
pin_in_read_high ( Pio volatile * pio_n, std::uint32_t pin_mask_n )
{
    return ( ( pio_n->PIO_PDSR & pin_mask_n ) != 0 );
}

} // namespace adue::pio
