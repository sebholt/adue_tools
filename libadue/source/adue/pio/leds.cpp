/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#include <adue/pio/leds.hpp>
#include <adue/pmc.hpp>

namespace adue::pio
{

void
init_led_p13 ( )
{
    adue::pmc_enable_periph_clk ( PIN_P13::peripheral_id() );
    PIN_P13::setup();
    set_led_p13 ( false );
}

void
init_led_rx ( )
{
    adue::pmc_enable_periph_clk ( PIN_RX::peripheral_id() );
    PIN_RX::setup();
    set_led_rx ( false );
}

void
init_led_tx ( )
{
    adue::pmc_enable_periph_clk ( PIN_TX::peripheral_id() );
    PIN_TX::setup();
    set_led_tx ( false );
}

void
init_leds ( )
{
    init_led_p13();
    init_led_rx();
    init_led_tx();
}


} // namespace adue::pio
