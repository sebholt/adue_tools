/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#pragma once

#include <adue/pio/pio.hpp>

namespace adue::pio
{

struct PioA
{
    static Pio volatile *
    pio ()
    {
        return PIOA;
    }

    static constexpr std::uint32_t
    peripheral_id ()
    {
        return ID_PIOA;
    }
};

struct PioB
{
    static Pio volatile *
    pio ()
    {
        return PIOB;
    }

    static constexpr std::uint32_t
    peripheral_id ()
    {
        return ID_PIOB;
    }
};

struct PioC
{
    static Pio volatile *
    pio ()
    {
        return PIOC;
    }

    static constexpr std::uint32_t
    peripheral_id ()
    {
        return ID_PIOC;
    }
};

struct PioD
{
    static Pio volatile *
    pio ()
    {
        return PIOD;
    }

    static constexpr std::uint32_t
    peripheral_id ()
    {
        return ID_PIOD;
    }
};

/// @brief Holds A Pio pointer and a pin bit mask.
///
struct Pin
{
    void
    reset ( Pio volatile * pio_n, std::uint32_t mask_n )
    {
        _pio = pio_n;
        _mask = mask_n;
    }

    // -- Pio

    Pio volatile *
    pio () const
    {
        return _pio;
    }

    void
    set_pio ( Pio volatile * pio_n )
    {
        _pio = pio_n;
    }

    // -- Mask

    std::uint32_t
    mask () const
    {
        return _mask;
    }

    void
    set_mask ( std::uint32_t mask_n )
    {
        _mask = mask_n;
    }

    Pio volatile * _pio;
    std::uint32_t _mask;
};

/// @brief Holds A Pio pointer and a pin bit mask.
///
struct Pin_Input : public Pin
{
    // -- Hardware register setup

    void
    setup () const
    {
        adue::pio::setup_as_input ( pio (), mask () );
    }

    // -- Reading

    bool
    read () const
    {
        return pin_in_read_high ( pio (), mask () );
    }

    // -- Comparison operators

    bool
    operator== ( const Pin_Input & pin_n ) const
    {
        return ( ( _pio == pin_n._pio ) && ( _mask == pin_n._mask ) );
    }

    bool
    operator!= ( const Pin_Input & pin_n ) const
    {
        return ( ( _pio != pin_n._pio ) || ( _mask != pin_n._mask ) );
    }
};

/// @brief Holds A Pio pointer and a pin bit mask.
///
struct Pin_Output : public Pin
{
    // -- Hardware register setup

    void
    setup () const
    {
        adue::pio::setup_as_output ( pio (), mask () );
    }

    void
    set_peripheral_b ( bool peripheral_b_n ) const
    {
        if ( peripheral_b_n ) {
            pio ()->PIO_ABSR |= mask (); // Select peripheral B
        } else {
            pio ()->PIO_ABSR &= ~mask (); // Select peripheral A
        }
    }

    void
    set_peripheral_controlled ( bool flag_n ) const
    {
        if ( flag_n ) {
            pio ()->PIO_PDR = mask (); // Disable PIO control -> peripheral
        } else {
            pio ()->PIO_PER = mask (); // Enable PIO control
        }
    }

    void
    setup_peripheral_a () const
    {
        setup ();
        set_peripheral_b ( false );
        set_peripheral_controlled ( true );
    }

    void
    setup_peripheral_b () const
    {
        setup ();
        set_peripheral_b ( true );
        set_peripheral_controlled ( true );
    }

    // -- Output value

    bool
    is_high () const
    {
        return pin_out_is_high ( pio (), mask () );
    }

    void
    set_low () const
    {
        pin_set_low ( pio (), mask () );
    }

    void
    set_high () const
    {
        pin_set_high ( pio (), mask () );
    }

    void
    set ( bool flag_n ) const
    {
        pin_set ( pio (), mask (), flag_n );
    }

    void
    toggle () const
    {
        pin_toggle ( pio (), mask () );
    }

    // -- Comparison operators

    bool
    operator== ( const Pin_Output & pin_n ) const
    {
        return ( ( _pio == pin_n._pio ) && ( _mask == pin_n._mask ) );
    }

    bool
    operator!= ( const Pin_Output & pin_n ) const
    {
        return ( ( _pio != pin_n._pio ) || ( _mask != pin_n._mask ) );
    }
};

/// @brief Compile time Pin
///
template < class PRT_, std::uint32_t MASK_ >
struct Pin_T
{
    // -- Accessors

    static constexpr Pio volatile *
    pio ()
    {
        return PRT_::pio ();
    }

    static constexpr std::uint32_t
    mask ()
    {
        return MASK_;
    }

    static constexpr std::uint32_t
    peripheral_id ()
    {
        return PRT_::peripheral_id ();
    }
};

/// @brief Compile time input pin
///
template < class PRT_, std::uint32_t MASK_ >
struct Pin_Input_T : public Pin_T< PRT_, MASK_ >
{
    // -- Accessors

    static constexpr Pio volatile *
    pio ()
    {
        return PRT_::pio ();
    }

    static constexpr std::uint32_t
    mask ()
    {
        return MASK_;
    }

    static constexpr std::uint32_t
    peripheral_id ()
    {
        return PRT_::peripheral_id ();
    }

    // -- Hardware register setup

    static void
    setup ()
    {
        adue::pio::setup_as_input ( pio (), mask () );
    }

    // -- Input pin

    static bool
    read ()
    {
        return pin_in_read_high ( pio (), mask () );
    }
};

/// @brief Compile time output pin
///
template < class PRT_, std::uint32_t MASK_ >
struct Pin_Output_T : public Pin_T< PRT_, MASK_ >
{
    // -- Accessors

    static constexpr Pio volatile *
    pio ()
    {
        return PRT_::pio ();
    }

    static constexpr std::uint32_t
    mask ()
    {
        return MASK_;
    }

    static constexpr std::uint32_t
    peripheral_id ()
    {
        return PRT_::peripheral_id ();
    }

    // -- Pin setup

    static void
    setup ()
    {
        adue::pio::setup_as_output ( pio (), mask () );
    }

    static void
    set_peripheral_b ( bool peripheral_b_n )
    {
        if ( peripheral_b_n ) {
            pio ()->PIO_ABSR |= mask (); // Select peripheral B
        } else {
            pio ()->PIO_ABSR &= ~mask (); // Select peripheral A
        }
    }

    static void
    set_peripheral_controlled ( bool flag_n )
    {
        if ( flag_n ) {
            pio ()->PIO_PDR = mask (); // Disable PIO control -> peripheral
        } else {
            pio ()->PIO_PER = mask (); // Enable PIO control
        }
    }

    static void
    setup_peripheral_a ()
    {
        setup ();
        set_peripheral_b ( false );
        set_peripheral_controlled ( true );
    }

    static void
    setup_peripheral_b ()
    {
        setup ();
        set_peripheral_b ( true );
        set_peripheral_controlled ( true );
    }

    // -- Output pin

    static bool
    is_high ()
    {
        return pin_out_is_high ( pio (), mask () );
    }

    static void
    set_low ()
    {
        pin_set_low ( pio (), mask () );
    }

    static void
    set_high ()
    {
        pin_set_high ( pio (), mask () );
    }

    static void
    set ( bool flag_n )
    {
        pin_set ( pio (), mask (), flag_n );
    }

    static void
    toggle ()
    {
        pin_toggle ( pio (), mask () );
    }
};

} // namespace adue::pio
