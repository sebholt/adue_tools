/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#include <adue/pio/pio.hpp>

namespace adue::pio
{

void
setup_as_output ( Pio volatile * port_n, std::uint32_t mask_n )
{
    port_n->PIO_OWDR = mask_n; // Disable parallel port writes
    port_n->PIO_IDR = mask_n;  // Disable interrupt generation
    port_n->PIO_MDDR = mask_n; // Disable multi driver (open drain) mode
    port_n->PIO_PUDR = mask_n; // Disable pull up
    port_n->PIO_CODR = mask_n; // Clear output value (low state)
    port_n->PIO_OER = mask_n;  // Enable port as output
    // port_n->PIO_ABSR &= ~mask_n; // Select peripheral A
    port_n->PIO_PER = mask_n; // Select output controlled by the PIO controller
}

void
setup_as_output_peripheral_a ( Pio volatile * port_n, std::uint32_t mask_n )
{
    port_n->PIO_OWDR = mask_n;   // Disable parallel port writes
    port_n->PIO_IDR = mask_n;    // Disable interrupt generation
    port_n->PIO_MDDR = mask_n;   // Disable multi driver (open drain) mode
    port_n->PIO_PUDR = mask_n;   // Disable pull up
    port_n->PIO_CODR = mask_n;   // Clear output value (low state)
    port_n->PIO_ODR = mask_n;    // Disable port output
    port_n->PIO_ABSR &= ~mask_n; // Select peripheral A
    port_n->PIO_PDR = mask_n;    // Select output controlled by the peripheral
}

void
setup_as_output_peripheral_b ( Pio volatile * port_n, std::uint32_t mask_n )
{
    port_n->PIO_OWDR = mask_n;  // Disable parallel port writes
    port_n->PIO_IDR = mask_n;   // Disable interrupt generation
    port_n->PIO_MDDR = mask_n;  // Disable multi driver (open drain) mode
    port_n->PIO_PUDR = mask_n;  // Disable pull up
    port_n->PIO_CODR = mask_n;  // Clear output value (low state)
    port_n->PIO_OER = mask_n;   // Enable port as output
    port_n->PIO_ABSR |= mask_n; // Select peripheral B
    port_n->PIO_PDR = mask_n;   // Select output controlled by the peripheral
}

void
setup_as_input ( Pio volatile * port_n, std::uint32_t mask_n )
{
    port_n->PIO_OWDR = mask_n; // Disable parallel port writes
    port_n->PIO_ODR = mask_n;  // Disable port output
    port_n->PIO_IFDR = mask_n; // Disable input filtering
    port_n->PIO_CODR = mask_n; // Clear output data
    port_n->PIO_IDR = mask_n;  // Disable interrupt
    port_n->PIO_MDDR = mask_n; // Disable port multi driver
    port_n->PIO_PUDR = mask_n; // Disable pull up
    // Don't write to the peripheral select register
    // port_n->PIO_ABSR &= ~mask_n; // Select peripheral A
    port_n->PIO_PER = mask_n; // Select output controlled by the PIO controller
}

} // namespace adue::pio
