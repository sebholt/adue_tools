/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#pragma once

namespace adue
{

/// @brief A dummy function that does nothing
extern void
no_op ();

} // namespace adue
