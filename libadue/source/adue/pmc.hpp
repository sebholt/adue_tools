/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#pragma once

#include <cstdint>

namespace adue
{

std::uint32_t
pmc_enable_periph_clk ( std::uint32_t ul_id );

std::uint32_t
pmc_disable_periph_clk ( std::uint32_t ul_id );

void
pmc_enable_interrupt ( std::uint32_t ul_sources );

void
pmc_disable_interrupt ( std::uint32_t ul_sources );

std::uint32_t
pmc_get_interrupt_mask ();

std::uint32_t
pmc_get_status ();

void
pmc_set_writeprotect ( std::uint32_t ul_enable );

std::uint32_t
pmc_get_writeprotect_status ();

void
pmc_enable_upll_clock ();

void
pmc_disable_upll_clock ();

void
pmc_enable_udpck ();

void
pmc_disable_udpck ();

void
pmc_switch_udpck_to_upllck ( std::uint32_t ul_usbdiv );

void
pmc_enable_udpck ();

} // namespace adue
