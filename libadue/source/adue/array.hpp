/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#pragma once

#include <cstdint>

namespace adue
{

template < typename TP, std::size_t SI >
struct Array
{
    // --- Size

    static constexpr std::size_t
    size ()
    {
        return SI;
    }

    // --- Data

    constexpr TP *
    data ()
    {
        return _data;
    }

    constexpr TP const *
    data () const
    {
        return _data;
    }

    // --- Iteration

    constexpr TP *
    begin ()
    {
        return _data;
    }

    constexpr TP const *
    begin () const
    {
        return _data;
    }

    constexpr TP const *
    cbegin () const
    {
        return _data;
    }

    constexpr TP *
    end ()
    {
        return ( _data + SI );
    }

    constexpr TP const *
    end () const
    {
        return ( _data + SI );
    }

    constexpr TP const *
    cend () const
    {
        return ( _data + SI );
    }

    void
    fill ( TP const & value_n )
    {
        for ( auto & item : *this ) {
            item = value_n;
        }
    }

    constexpr TP & operator[] ( std::size_t index_n )
    {
        return _data[ index_n ];
    }

    constexpr TP const & operator[] ( std::size_t index_n ) const
    {
        return _data[ index_n ];
    }

    TP _data[ SI ];
};

} // namespace adue
