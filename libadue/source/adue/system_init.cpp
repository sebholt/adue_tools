/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#include <sam3x8e.h>
#include <adue/no_op.hpp>
#include <adue/system_init.hpp>

namespace adue
{

void
system_init ()
{
    SystemInit ();

    // -- System ticks
    // Set Systick to 1ms interval, common to all SAM3 variants
    if ( SysTick_Config ( SystemCoreClock / 1000 ) ) {
        // Block
        while ( true )
            ;
    }

    // -- Watchdog
    // Disable watchdog
    {
        WDT->WDT_MR = WDT_MR_WDDIS;
    }

    // TODO: Init libc on demand
}

} // namespace adue
