/// Arduino Due EFrog controller implementation.
/// \copyright See LICENSE-efrog.txt file

#pragma once

#include <algorithm>
#include <cstdint>

namespace adue
{

/// @brief Ring buffer with a static size
///
template < class IT_, std::uint32_t CAP_ >
class Ring_Buffer
{
    public:
    // -- Types
    struct Tile
    {
        IT_ * data;
        std::uint32_t size;
    };

    // -- Methods

    void
    reset ()
    {
        _size = 0;
        _front_index = 0;
    }

    void
    clear ()
    {
        _size = 0;
        _front_index = 0;
    }

    static constexpr std::uint32_t
    capacity ()
    {
        return CAP_;
    }

    std::uint32_t
    size () const
    {
        return _size;
    }

    std::uint32_t
    size_free () const
    {
        return ( capacity () - _size );
    }

    bool
    is_empty () const
    {
        return ( _size == 0 );
    }

    bool
    is_full () const
    {
        return ( _size == capacity () );
    }

    IT_ &
    back ()
    {
        return _items[ ( _front_index + _size - 1 ) % capacity () ];
    }

    const IT_ &
    back () const
    {
        return _items[ ( _front_index + _size - 1 ) % capacity () ];
    }

    void
    push_back_not_full ( const IT_ & item_n )
    {
        _items[ ( _front_index + _size ) % capacity () ] = item_n;
        ++_size;
    }

    void
    push_back ( const IT_ & item_n )
    {
        _items[ ( _front_index + _size ) % capacity () ] = item_n;
        // Drop the first item or increment size
        if ( is_full () ) {
            increment_front_index ();
        } else {
            ++_size;
        }
    }

    /// @brief Writes the given data to the end of the ring buffer without a
    ///        size check.
    void
    write_back_unchecked ( const IT_ * data_n, std::uint32_t size_n );

    IT_ &
    front ()
    {
        return _items[ _front_index ];
    }

    const IT_ &
    front () const
    {
        return _items[ _front_index ];
    }

    void
    pop_front_not_empty ()
    {
        --_size;
        increment_front_index ();
    }

    void
    pop_front_unchecked ( std::uint32_t num_n )
    {
        _size -= num_n;
        increment_front_index ( num_n );
    }

    void
    pop_front ()
    {
        if ( !is_empty () ) {
            pop_front_not_empty ();
        }
    }

    IT_ &
    item ( std::uint32_t index_n )
    {
        return _items[ ( _front_index + index_n ) % capacity () ];
    }

    const IT_ &
    item ( std::uint32_t index_n ) const
    {
        return _items[ ( _front_index + index_n ) % capacity () ];
    }

    // -- Tile interface

    /// @brief The first tile of busy items (might contain all items)
    Tile
    tile_busy ()
    {
        return Tile{ &_items[ _front_index ],
                     std::min ( _size, capacity () - _front_index ) };
    }

    /// @brief The first tile of unused items (might contain all unused
    /// items)
    Tile
    tile_free ()
    {
        std::uint32_t const end_index = ( _front_index + _size ) % capacity ();
        return Tile{ &_items[ end_index ],
                     capacity () - std::max ( _size, end_index ) };
    }

    void
    increment_front_index ( std::uint32_t delta_n = 1 )
    {
        _front_index = ( _front_index + delta_n ) % capacity ();
    }

    void
    increment_size ( std::uint32_t delta_n )
    {
        _size += delta_n;
    }

    private:
    std::uint32_t _size = 0;
    std::uint32_t _front_index = 0;
    IT_ _items[ CAP_ ];
};

template < class IT_, std::uint32_t CAP_ >
void
Ring_Buffer< IT_, CAP_ >::write_back_unchecked ( const IT_ * data_n,
                                                 std::uint32_t size_n )
{
    std::uint32_t const end_index = ( _front_index + _size ) % capacity ();
    std::uint32_t const end_tile_size =
        capacity () - std::max ( _size, end_index );
    // Copy the data begin to the end tile
    {
        std::uint32_t const size_to_copy = std::min ( size_n, end_tile_size );
        IT_ * end_tile = &_items[ end_index ];
        for ( std::uint32_t ii = 0; ii != size_to_copy; ++ii ) {
            end_tile[ ii ] = data_n[ ii ];
        }
    }
    // Copy the rest to the buffer begin
    if ( end_tile_size < size_n ) {
        std::uint32_t const size_to_copy = size_n - end_tile_size;
        IT_ const * data_end = &data_n[ end_tile_size ];
        for ( std::uint32_t ii = 0; ii != size_to_copy; ++ii ) {
            _items[ ii ] = data_end[ ii ];
        }
    }
    // Increment size
    _size += size_n;
}

} // namespace adue
