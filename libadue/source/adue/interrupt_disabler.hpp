/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#pragma once

#include <sam3x8e.h>

namespace adue
{

/// @brief disables the interrupt on construction and reenables it on
/// destruction
template < IRQn_Type IRQN_ >
struct Interrupt_Disabler_T
{
    Interrupt_Disabler_T ()
    { // Disable interrupt
        NVIC_DisableIRQ ( IRQN_ );
    }

    ~Interrupt_Disabler_T ()
    {
        // Enable interrupt
        NVIC_EnableIRQ ( IRQN_ );
    }
};

} // namespace adue
