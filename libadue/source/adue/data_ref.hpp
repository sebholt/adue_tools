/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#pragma once

#include <cstdint>

namespace adue
{

/// @brief Data Reference
class Data_Ref
{
    public:
    Data_Ref ()
    : _data ( nullptr )
    , _size ( 0 )
    {
    }

    Data_Ref ( std::uint8_t const * data_n, std::uint32_t size_n )
    : _data ( data_n )
    , _size ( size_n )
    {
    }

    void
    clear ()
    {
        _data = nullptr;
        _size = 0;
    }

    std::uint8_t const *
    data () const
    {
        return _data;
    }

    void
    set_data ( std::uint8_t const * data_n )
    {
        _data = data_n;
    }

    std::uint32_t
    size () const
    {
        return _size;
    }

    void
    set_size ( std::uint32_t size_n )
    {
        _size = size_n;
    }

    void
    set ( std::uint8_t const * data_n, std::uint32_t size_n )
    {
        _data = data_n;
        _size = size_n;
    }

    bool
    is_valid () const
    {
        return ( ( data () != nullptr ) && ( size () != 0 ) );
    }

    bool
    operator== ( Data_Ref const & dref_n ) const
    {
        return ( ( data () == dref_n.data () ) &&
                 ( size () == dref_n.size () ) );
    }

    bool
    operator!= ( Data_Ref const & dref_n ) const
    {
        return ( ( data () != dref_n.data () ) ||
                 ( size () != dref_n.size () ) );
    }

    private:
    std::uint8_t const * _data;
    std::uint32_t _size;
};

} // namespace adue
