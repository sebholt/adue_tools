/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#pragma once

#include <cstdint>

namespace adue
{

void
system_init ();

} // namespace adue
