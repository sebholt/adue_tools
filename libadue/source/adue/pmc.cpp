/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#include <sam3x8e.h>
#include <adue/pmc.hpp>

#define MAX_PERIPH_ID 44
#define PMC_WPMR_WPKEY_VALUE PMC_WPMR_WPKEY ( (std::uint32_t)0x504D43 )

namespace adue
{

/**
 * \brief Enable the specified peripheral clock.
 *
 * \note The ID must NOT be shifted (i.e., 1 << ID_xxx).
 *
 * \param ul_id Peripheral ID (ID_xxx).
 *
 * \retval 0 Success.
 * \retval 1 Invalid parameter.
 */
std::uint32_t
pmc_enable_periph_clk ( std::uint32_t ul_id )
{
    if ( ul_id > MAX_PERIPH_ID ) {
        return 1;
    }

    if ( ul_id < 32 ) {
        if ( ( PMC->PMC_PCSR0 & ( 1u << ul_id ) ) != ( 1u << ul_id ) ) {
            PMC->PMC_PCER0 = 1 << ul_id;
        }
    } else {
        ul_id -= 32;
        if ( ( PMC->PMC_PCSR1 & ( 1u << ul_id ) ) != ( 1u << ul_id ) ) {
            PMC->PMC_PCER1 = 1 << ul_id;
        }
    }

    return 0;
}

/**
 * \brief Disable the specified peripheral clock.
 *
 * \note The ID must NOT be shifted (i.e., 1 << ID_xxx).
 *
 * \param ul_id Peripheral ID (ID_xxx).
 *
 * \retval 0 Success.
 * \retval 1 Invalid parameter.
 */
std::uint32_t
pmc_disable_periph_clk ( std::uint32_t ul_id )
{
    if ( ul_id > MAX_PERIPH_ID ) {
        return 1;
    }

    if ( ul_id < 32 ) {
        if ( ( PMC->PMC_PCSR0 & ( 1u << ul_id ) ) == ( 1u << ul_id ) ) {
            PMC->PMC_PCDR0 = 1 << ul_id;
        }
    } else {
        ul_id -= 32;
        if ( ( PMC->PMC_PCSR1 & ( 1u << ul_id ) ) == ( 1u << ul_id ) ) {
            PMC->PMC_PCDR1 = 1 << ul_id;
        }
    }
    return 0;
}

/**
 * \brief Enable PMC interrupts.
 *
 * \param ul_sources Interrupt sources bit map.
 */
void
pmc_enable_interrupt ( std::uint32_t ul_sources )
{
    PMC->PMC_IER = ul_sources;
}

/**
 * \brief Disable PMC interrupts.
 *
 * \param ul_sources Interrupt sources bit map.
 */
void
pmc_disable_interrupt ( std::uint32_t ul_sources )
{
    PMC->PMC_IDR = ul_sources;
}

/**
 * \brief Get PMC interrupt mask.
 *
 * \return The interrupt mask value.
 */
std::uint32_t
pmc_get_interrupt_mask ()
{
    return PMC->PMC_IMR;
}

/**
 * \brief Get current status.
 *
 * \return The current PMC status.
 */
std::uint32_t
pmc_get_status ()
{
    return PMC->PMC_SR;
}

/**
 * \brief Enable or disable write protect of PMC registers.
 *
 * \param ul_enable 1 to enable, 0 to disable.
 */
void
pmc_set_writeprotect ( std::uint32_t ul_enable )
{
    if ( ul_enable ) {
        PMC->PMC_WPMR = PMC_WPMR_WPKEY_VALUE | PMC_WPMR_WPEN;
    } else {
        PMC->PMC_WPMR = PMC_WPMR_WPKEY_VALUE;
    }
}

/**
 * \brief Return write protect status.
 *
 * \retval 0 Protection disabled.
 * \retval 1 Protection enabled.
 */
std::uint32_t
pmc_get_writeprotect_status ()
{
    return PMC->PMC_WPMR & PMC_WPMR_WPEN;
}

/**
 * \brief Enable UPLL clock.
 */
void
pmc_enable_upll_clock ()
{
    PMC->CKGR_UCKR = CKGR_UCKR_UPLLCOUNT ( 3 ) | CKGR_UCKR_UPLLEN;

    /* Wait UTMI PLL Lock Status */
    while ( !( PMC->PMC_SR & PMC_SR_LOCKU ) )
        ;
}

/**
 * \brief Disable UPLL clock.
 */
void
pmc_disable_upll_clock ()
{
    PMC->CKGR_UCKR &= ~CKGR_UCKR_UPLLEN;
}

/**
 * \brief Switch UDP (USB) clock source selection to UPLL clock.
 *
 * \param dw_usbdiv Clock divisor.
 */
void
pmc_switch_udpck_to_upllck ( std::uint32_t ul_usbdiv )
{
    PMC->PMC_USB = PMC_USB_USBS | PMC_USB_USBDIV ( ul_usbdiv );
}

/**
 * \brief Enable UDP (USB) clock.
 */
void
pmc_enable_udpck ()
{
    PMC->PMC_SCER = PMC_SCER_UOTGCLK;
}

/**
 * \brief Disable UDP (USB) clock.
 */
void
pmc_disable_udpck ()
{
    PMC->PMC_SCDR = PMC_SCDR_UOTGCLK;
}

} // namespace adue
