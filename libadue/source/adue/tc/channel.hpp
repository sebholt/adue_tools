/// Arduino Due EFrog controller implementation.
/// \copyright See LICENSE-efrog.txt file

#pragma once

#include <cstdint>
#include <sam3x8e.h>

namespace adue::tc
{

/// @brief PWM block channel controller
///
class Block_Channel
{
    public:
    // -- Initialization

    void
    init ( std::uint8_t block_index_n, std::uint8_t block_channel_index_n )
    {
        _block = reinterpret_cast< Tc volatile * > (
            reinterpret_cast< std::uint8_t * > ( TC0 ) +
            ( block_index_n * 0x4000 ) );
        _channel = &( _block->TC_CHANNEL[ block_channel_index_n ] );
        _block_index = block_index_n;
        _block_channel_index = block_channel_index_n;
        _global_channel_index = ( block_index_n * 3 + block_channel_index_n );
    }

    // -- Block

    Tc volatile *
    block () const
    {
        return _block;
    }

    std::uint8_t
    block_index () const
    {
        return _block_index;
    }

    // -- Channel

    TcChannel volatile *
    channel () const
    {
        return _channel;
    }

    std::uint8_t
    block_channel_index () const
    {
        return _block_channel_index;
    }

    // -- Global channel index

    std::uint8_t
    global_channel_index () const
    {
        return _global_channel_index;
    }

    IRQn_Type
    irqn () const
    {
        return static_cast< IRQn_Type > (
            static_cast< std::uint32_t > ( TC0_IRQn ) +
            global_channel_index () );
    }

    std::uint32_t
    peripheral_id () const
    {
        return ( std::uint32_t ( ID_TC0 ) + global_channel_index () );
    }

    // -- Setup registers

    std::uint32_t
    mode () const
    {
        return channel ()->TC_CMR;
    }

    void
    set_mode ( std::uint32_t mode_n )
    {
        channel ()->TC_CMR = mode_n;
    }

    // -- Status registers

    std::uint32_t
    status () const
    {
        return channel ()->TC_SR;
    }

    std::uint32_t
    counter () const
    {
        return channel ()->TC_CV;
    }

    // -- Match registers

    std::uint32_t
    ra () const
    {
        return channel ()->TC_RA;
    }

    void
    ra_set ( std::uint32_t val_n )
    {
        channel ()->TC_RA = val_n;
    }

    std::uint32_t
    rb () const
    {
        return channel ()->TC_RB;
    }

    void
    rb_set ( std::uint32_t val_n )
    {
        channel ()->TC_RB = val_n;
    }

    std::uint32_t
    rc () const
    {
        return channel ()->TC_RC;
    }

    void
    rc_set ( std::uint32_t val_n )
    {
        channel ()->TC_RC = val_n;
    }

    // -- Interrupt registers

    void
    interrupt_enable ( std::uint32_t mask_n )
    {
        channel ()->TC_IER = mask_n;
    }

    void
    interrupt_disable ( std::uint32_t mask_n )
    {
        channel ()->TC_IDR = mask_n;
    }

    std::uint32_t
    interrupt_mask () const
    {
        return channel ()->TC_IMR;
    }

    void
    interrupt_enable_ra ()
    {
        interrupt_enable ( TC_IER_CPAS );
    }

    void
    interrupt_disable_ra ()
    {
        interrupt_disable ( TC_IDR_CPAS );
    }

    void
    interrupt_enable_rb ()
    {
        interrupt_enable ( TC_IER_CPBS );
    }

    void
    interrupt_disable_rb ()
    {
        interrupt_disable ( TC_IDR_CPBS );
    }

    void
    interrupt_enable_rc ()
    {
        interrupt_enable ( TC_IER_CPCS );
    }

    void
    interrupt_disable_rc ()
    {
        interrupt_disable ( TC_IDR_CPCS );
    }

    // -- Start / stop

    void
    clock_enable ()
    {
        channel ()->TC_CCR = TC_CCR_CLKEN;
    }

    void
    clock_disable ()
    {
        channel ()->TC_CCR = TC_CCR_CLKDIS;
    }

    /// @brief Enables and starts the clock
    void
    clock_enable_start ()
    {
        channel ()->TC_CCR = ( TC_CCR_CLKEN | TC_CCR_SWTRG );
    }

    /// @brief Starts the clock only if it is enabled
    void
    trigger_start ()
    {
        channel ()->TC_CCR = TC_CCR_SWTRG;
    }

    private:
    TcChannel volatile * _channel;
    Tc volatile * _block;
    std::uint8_t _block_index;
    std::uint8_t _block_channel_index;
    std::uint8_t _global_channel_index;
};

/// @brief PWM global channel controller
///
class Channel : public Block_Channel
{
    public:
    // -- Initialization

    void
    init ( std::uint8_t global_channel_index_n )
    {
        Block_Channel::init ( global_channel_index_n / 3,
                              global_channel_index_n % 3 );
    }
};

/// @brief PWM block channel controller
///
template < std::uint8_t BIDX_, std::uint8_t CIDX_ >
class Block_Channel_T
{
    public:
    // -- Block

    static constexpr std::uint8_t
    block_index ()
    {
        static_assert ( BIDX_ < 3 );
        return BIDX_;
    }

    static Tc volatile *
    block ()
    {
        return reinterpret_cast< Tc volatile * > (
            reinterpret_cast< std::uint8_t * > ( TC0 ) +
            ( block_index () * 0x4000 ) );
    }

    // -- Channel

    static constexpr std::uint8_t
    index ()
    {
        static_assert ( CIDX_ < 3 );
        return CIDX_;
    }

    static TcChannel volatile *
    channel ()
    {
        return &block ()->TC_CHANNEL[ index () ];
    }

    // -- Global channel

    static constexpr std::uint8_t
    global_index ()
    {
        return block_index () * std::uint8_t ( 3 ) + index ();
    }

    static constexpr IRQn_Type
    irqn ()
    {
        return static_cast< IRQn_Type > (
            static_cast< std::uint32_t > ( TC0_IRQn ) + global_index () );
    }

    static constexpr std::uint32_t
    peripheral_id ()
    {
        return ( std::uint32_t ( ID_TC0 ) + global_index () );
    }

    // -- Setup registers

    static constexpr std::uint32_t
    mode ()
    {
        return channel ()->TC_CMR;
    }

    static void
    set_mode ( std::uint32_t mode_n )
    {
        channel ()->TC_CMR = mode_n;
    }

    // -- Status registers

    static std::uint32_t
    status ()
    {
        return channel ()->TC_SR;
    }

    static std::uint32_t
    counter ()
    {
        return channel ()->TC_CV;
    }

    // -- Match registers

    static std::uint32_t
    ra ()
    {
        return channel ()->TC_RA;
    }

    static void
    ra_set ( std::uint32_t val_n )
    {
        channel ()->TC_RA = val_n;
    }

    static std::uint32_t
    rb ()
    {
        return channel ()->TC_RB;
    }

    static void
    rb_set ( std::uint32_t val_n )
    {
        channel ()->TC_RB = val_n;
    }

    static std::uint32_t
    rc ()
    {
        return channel ()->TC_RC;
    }

    static void
    rc_set ( std::uint32_t val_n )
    {
        channel ()->TC_RC = val_n;
    }

    // -- Interrupt registers

    static void
    interrupt_enable ( std::uint32_t mask_n )
    {
        channel ()->TC_IER = mask_n;
    }

    static void
    interrupt_disable ( std::uint32_t mask_n )
    {
        channel ()->TC_IDR = mask_n;
    }

    static std::uint32_t
    interrupt_mask ()
    {
        return channel ()->TC_IMR;
    }

    static void
    interrupt_enable_ra ()
    {
        interrupt_enable ( TC_IER_CPAS );
    }

    static void
    interrupt_disable_ra ()
    {
        interrupt_disable ( TC_IDR_CPAS );
    }

    static void
    interrupt_enable_rb ()
    {
        interrupt_enable ( TC_IER_CPBS );
    }

    static void
    interrupt_disable_rb ()
    {
        interrupt_disable ( TC_IDR_CPBS );
    }

    static void
    interrupt_enable_rc ()
    {
        interrupt_enable ( TC_IER_CPCS );
    }

    static void
    interrupt_disable_rc ()
    {
        interrupt_disable ( TC_IDR_CPCS );
    }

    // -- Start / stop

    static void
    clock_enable ()
    {
        channel ()->TC_CCR = TC_CCR_CLKEN;
    }

    static void
    clock_disable ()
    {
        channel ()->TC_CCR = TC_CCR_CLKDIS;
    }

    /// @brief Enables and starts the clock
    static void
    clock_enable_start ()
    {
        channel ()->TC_CCR = ( TC_CCR_CLKEN | TC_CCR_SWTRG );
    }

    /// @brief Starts the clock only if it is enabled
    static void
    trigger_start ()
    {
        channel ()->TC_CCR = TC_CCR_SWTRG;
    }
};

template < std::uint32_t IDX_ >
class Channel_T : public Block_Channel_T< IDX_ / 3, IDX_ % 3 >
{
};

} // namespace adue::tc
