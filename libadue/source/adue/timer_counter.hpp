/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#pragma once

#include <sam3x8e.h>
#include <adue/pmc.hpp>

namespace adue
{

template < std::uint32_t TIDX >
class Timer_Counter
{
    public:
    static void
    init_mode ( std::uint32_t mode_n );

    static void
    clock_enable ()
    {
        channel ().TC_CCR = TC_CCR_CLKEN;
    }

    static void
    clock_disable ()
    {
        channel ().TC_CCR = TC_CCR_CLKDIS;
    }

    static void
    start ()
    {
        // Enable and reset value
        channel ().TC_CCR = ( TC_CCR_CLKEN | TC_CCR_SWTRG );
    }

    static void
    stop ()
    {
        channel ().TC_CCR = TC_CCR_CLKDIS;
    }

    static Tc &
    tcounter ();

    static TcChannel &
    channel ()
    {
        return tcounter ().TC_CHANNEL[ ( TIDX % 3 ) ];
    }

    static IRQn_Type
    irqn_type ();

    static std::uint32_t
    status ()
    {
        return channel ().TC_SR;
    }

    static std::uint32_t
    counter_value ()
    {
        return channel ().TC_CV;
    }

    static std::uint32_t
    interrupts_rx_disable ()
    {
        const std::uint32_t ir_bits ( TC_IMR_CPAS | TC_IMR_CPBS | TC_IMR_CPCS );
        const std::uint32_t res ( channel ().TC_IMR & ir_bits );
        channel ().TC_IDR = ir_bits;
        return res;
    }

    static void
    interrupts_rx_enable ( std::uint32_t flags_n )
    {
        const std::uint32_t ir_bits ( TC_IMR_CPAS | TC_IMR_CPBS | TC_IMR_CPCS );
        channel ().TC_IER = flags_n & ir_bits;
    }

    static void
    enable_interrupt_ra ()
    {
        channel ().TC_IER = TC_IER_CPAS;
    }

    static void
    disable_interrupt_ra ()
    {
        channel ().TC_IDR = TC_IDR_CPAS;
    }

    static std::uint32_t
    ra ()
    {
        return channel ().TC_RA;
    }

    static void
    set_ra ( std::uint32_t val_n )
    {
        channel ().TC_RA = val_n;
    }

    static void
    enable_interrupt_rb ()
    {
        channel ().TC_IER = TC_IER_CPBS;
    }

    static void
    disable_interrupt_rb ()
    {
        channel ().TC_IDR = TC_IDR_CPBS;
    }

    static std::uint32_t
    rb ()
    {
        return channel ().TC_RB;
    }

    static void
    set_rb ( std::uint32_t val_n )
    {
        channel ().TC_RB = val_n;
    }

    static void
    enable_interrupt_rc ()
    {
        channel ().TC_IER = TC_IER_CPCS;
    }

    static void
    disable_interrupt_rc ()
    {
        channel ().TC_IDR = TC_IDR_CPCS;
    }

    static std::uint32_t
    rc ()
    {
        return channel ().TC_RC;
    }

    static void
    set_rc ( std::uint32_t val_n )
    {
        channel ().TC_RC = val_n;
    }
};

template <>
inline Tc &
Timer_Counter< 0 >::tcounter ()
{
    return ( *TC0 );
}
template <>
inline Tc &
Timer_Counter< 1 >::tcounter ()
{
    return ( *TC0 );
}
template <>
inline Tc &
Timer_Counter< 2 >::tcounter ()
{
    return ( *TC0 );
}
template <>
inline Tc &
Timer_Counter< 3 >::tcounter ()
{
    return ( *TC1 );
}
template <>
inline Tc &
Timer_Counter< 4 >::tcounter ()
{
    return ( *TC1 );
}
template <>
inline Tc &
Timer_Counter< 5 >::tcounter ()
{
    return ( *TC1 );
}
template <>
inline Tc &
Timer_Counter< 6 >::tcounter ()
{
    return ( *TC2 );
}
template <>
inline Tc &
Timer_Counter< 7 >::tcounter ()
{
    return ( *TC2 );
}
template <>
inline Tc &
Timer_Counter< 8 >::tcounter ()
{
    return ( *TC2 );
}

template <>
inline IRQn_Type
Timer_Counter< 0 >::irqn_type ()
{
    return TC0_IRQn;
}
template <>
inline IRQn_Type
Timer_Counter< 1 >::irqn_type ()
{
    return TC1_IRQn;
}
template <>
inline IRQn_Type
Timer_Counter< 2 >::irqn_type ()
{
    return TC2_IRQn;
}
template <>
inline IRQn_Type
Timer_Counter< 3 >::irqn_type ()
{
    return TC3_IRQn;
}
template <>
inline IRQn_Type
Timer_Counter< 4 >::irqn_type ()
{
    return TC4_IRQn;
}
template <>
inline IRQn_Type
Timer_Counter< 5 >::irqn_type ()
{
    return TC5_IRQn;
}
template <>
inline IRQn_Type
Timer_Counter< 6 >::irqn_type ()
{
    return TC6_IRQn;
}
template <>
inline IRQn_Type
Timer_Counter< 7 >::irqn_type ()
{
    return TC7_IRQn;
}
template <>
inline IRQn_Type
Timer_Counter< 8 >::irqn_type ()
{
    return TC8_IRQn;
}

template < std::uint32_t TIDX >
inline void
Timer_Counter< TIDX >::init_mode ( std::uint32_t mode_n )
{
    pmc_set_writeprotect ( false );
    pmc_enable_periph_clk ( (std::uint32_t)irqn_type () );

    {
        // Disable TC clock / Stop
        channel ().TC_CCR = TC_CCR_CLKDIS;
        // Disable interrupts
        channel ().TC_IDR = 0xFFFFFFFF;
        // Clear status register
        channel ().TC_SR;
        set_ra ( 0 );
        set_rb ( 0 );
        set_rc ( 1000 * 1000 );
        // Set mode
        channel ().TC_CMR = mode_n;
    }

    // Enable interrupt controller interrupts
    NVIC_EnableIRQ ( irqn_type () );
}

template < std::uint32_t TIDX >
class Timer_Counter_Fast : public adue::Timer_Counter< TIDX >
{
    // Public methods
    public:
    static void
    init ();

    static std::uint32_t
    ticks_per_us ();

    static std::uint32_t
    ticks_for_us ( std::uint32_t us_n );
};

template < std::uint32_t TIDX >
void
Timer_Counter_Fast< TIDX >::init ()
{
    adue::Timer_Counter< TIDX >::init_mode ( TC_CMR_WAVE | TC_CMR_WAVSEL_UP_RC |
                                             TC_CMR_TCCLKS_TIMER_CLOCK1 );
}

template < std::uint32_t TIDX >
inline std::uint32_t
Timer_Counter_Fast< TIDX >::ticks_per_us ()
{
    // The selected TIMER_CLOCK1 is the main clock / 2
    return ( ( F_CPU / 2ul ) / ( 1000000ul ) );
}

template < std::uint32_t TIDX >
inline std::uint32_t
Timer_Counter_Fast< TIDX >::ticks_for_us ( std::uint32_t us_n )
{
    return ( us_n * ticks_per_us () );
}

} // namespace adue
