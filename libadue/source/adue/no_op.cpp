/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#include <adue/no_op.hpp>

namespace adue
{

void
no_op ()
{
}

} // namespace adue
