/// Arduino Due PWM interrupt timing test.
/// \copyright See LICENSE-efrog.txt file

#pragma once

#include <cstdint>

namespace adue
{

static constexpr const std::uint32_t uint32_false = 0x00000000;
static constexpr const std::uint32_t uint32_true = 0xffffffff;

} // namespace adue
