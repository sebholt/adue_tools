/// libadue: C++ utility library for the Arduino Due.
/// \copyright See LICENSE-libadue.txt file

#pragma once

#include <sam3x8e.h>
#include <adue/pmc.hpp>

namespace adue
{

class Timer_Counter_Channel
{
    // Public types
    public:
    static const std::uint32_t num_timers = 3;
    static const std::uint32_t channels_per_timer = 3;

    static const std::uint32_t ir_bits =
        ( TC_IMR_COVFS | TC_IMR_LOVRS | TC_IMR_CPAS | TC_IMR_CPBS |
          TC_IMR_CPCS | TC_IMR_LDRAS | TC_IMR_LDRBS | TC_IMR_ETRGS );

    static const std::uint32_t ir_rx_bits =
        ( TC_IMR_CPAS | TC_IMR_CPBS | TC_IMR_CPCS );

    // Public methods
    public:
    void
    select_meta_channel ( std::uint32_t channel_index_n );

    void
    select_timer_channel ( std::uint32_t timer_index_n,
                           std::uint32_t channel_index_n );

    TcChannel *
    channel () const;

    Tc *
    timer_counter () const;

    std::uint32_t
    acquire_timer_index () const;

    std::uint32_t
    acquire_channel_index () const;

    std::uint32_t
    acquire_meta_channel_index () const;

    void
    setup_mode ( std::uint32_t mode_n, bool enable_interrupt_n );

    void
    enable ();

    void
    disable ();

    void
    software_trigger ();

    std::uint32_t
    status ();

    void
    clear_status ();

    std::uint32_t
    counter_value ();

    void
    set_counter_value ( std::uint32_t value_n );

    void
    interrupts_disable ();

    void
    interrupts_disable ( std::uint32_t & interupts_before_n );

    void
    interrupts_enable ( std::uint32_t flags_n );

    void
    interrupts_rx_disable ();

    void
    interrupts_rx_disable ( std::uint32_t & interupts_before_n );

    void
    interrupts_rx_enable ( std::uint32_t flags_n );

    void
    enable_interrupt_ra ();

    void
    disable_interrupt_ra ();

    std::uint32_t
    ra ();

    void
    set_ra ( std::uint32_t val_n );

    void
    enable_interrupt_rb ();

    void
    disable_interrupt_rb ();

    std::uint32_t
    rb ();

    void
    set_rb ( std::uint32_t val_n );

    void
    enable_interrupt_rc ();

    void
    disable_interrupt_rc ();

    std::uint32_t
    rc ();

    void
    set_rc ( std::uint32_t val_n );

    // Private attributes
    private:
    TcChannel * _channel;
    Tc * _timer_counter;
};

inline TcChannel *
Timer_Counter_Channel::channel () const
{
    return _channel;
}

inline Tc *
Timer_Counter_Channel::timer_counter () const
{
    return _timer_counter;
}

inline void
Timer_Counter_Channel::enable ()
{
    // Enable and reset value
    _channel->TC_CCR = TC_CCR_CLKEN;
}

inline void
Timer_Counter_Channel::disable ()
{
    // Disable counter
    _channel->TC_CCR = TC_CCR_CLKDIS;
}

inline void
Timer_Counter_Channel::software_trigger ()
{
    // Issue a software trigger
    _channel->TC_CCR = TC_CCR_SWTRG;
}

inline std::uint32_t
Timer_Counter_Channel::status ()
{
    return _channel->TC_SR;
}

inline void
Timer_Counter_Channel::clear_status ()
{
    const std::uint32_t sread ( _channel->TC_SR );
    (void)sread;
}

inline std::uint32_t
Timer_Counter_Channel::counter_value ()
{
    return _channel->TC_CV;
}

inline void
Timer_Counter_Channel::set_counter_value ( std::uint32_t value_n )
{
    _channel->TC_CV = value_n;
}

inline void
Timer_Counter_Channel::interrupts_disable ()
{
    // assume same bit positions in mask and disable register
    _channel->TC_IDR = ir_bits;
}

inline void
Timer_Counter_Channel::interrupts_disable (
    std::uint32_t & interrupts_before_n )
{
    // assume same bit positions in mask and disable register
    interrupts_before_n = ( _channel->TC_IMR & ir_bits );
    _channel->TC_IDR = ir_rx_bits;
}

inline void
Timer_Counter_Channel::interrupts_enable ( std::uint32_t flags_n )
{
    _channel->TC_IER = ( flags_n & ir_bits );
}

inline void
Timer_Counter_Channel::interrupts_rx_disable ()
{
    // assume same bit positions in mask and disable register
    _channel->TC_IDR = ir_rx_bits;
}

inline void
Timer_Counter_Channel::interrupts_rx_disable (
    std::uint32_t & interrupts_before_n )
{
    // assume same bit positions in mask and disable register
    interrupts_before_n = ( _channel->TC_IMR & ir_rx_bits );
    _channel->TC_IDR = ir_rx_bits;
}

inline void
Timer_Counter_Channel::interrupts_rx_enable ( std::uint32_t flags_n )
{
    _channel->TC_IER = ( flags_n & ir_rx_bits );
}

inline void
Timer_Counter_Channel::enable_interrupt_ra ()
{
    _channel->TC_IER = TC_IER_CPAS;
}

inline void
Timer_Counter_Channel::disable_interrupt_ra ()
{
    _channel->TC_IDR = TC_IDR_CPAS;
}

inline std::uint32_t
Timer_Counter_Channel::ra ()
{
    return _channel->TC_RA;
}

inline void
Timer_Counter_Channel::set_ra ( std::uint32_t val_n )
{
    _channel->TC_RA = val_n;
}

inline void
Timer_Counter_Channel::enable_interrupt_rb ()
{
    _channel->TC_IER = TC_IER_CPBS;
}

inline void
Timer_Counter_Channel::disable_interrupt_rb ()
{
    _channel->TC_IDR = TC_IDR_CPBS;
}

inline std::uint32_t
Timer_Counter_Channel::rb ()
{
    return _channel->TC_RB;
}

inline void
Timer_Counter_Channel::set_rb ( std::uint32_t val_n )
{
    _channel->TC_RB = val_n;
}

inline void
Timer_Counter_Channel::enable_interrupt_rc ()
{
    _channel->TC_IER = TC_IER_CPCS;
}

inline void
Timer_Counter_Channel::disable_interrupt_rc ()
{
    _channel->TC_IDR = TC_IDR_CPCS;
}

inline std::uint32_t
Timer_Counter_Channel::rc ()
{
    return _channel->TC_RC;
}

inline void
Timer_Counter_Channel::set_rc ( std::uint32_t val_n )
{
    _channel->TC_RC = val_n;
}

} // namespace adue
