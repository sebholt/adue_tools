/* ----------------------------------------------------------------------------
 *         Cortex-M3 dummy handlers
 * ----------------------------------------------------------------------------
 * Copyright (c) 2014, Sebastian Holtermann
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following condition is met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer below.
 *
 * DISCLAIMER: THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDERS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ----------------------------------------------------------------------------
 */

#include <sam3x8e.h>

#define MAX_PERIPH_ID 44

// Blinking counter ticks
const uint32_t blink_ticks = 100;
uint32_t blink_ticks_accu;
uint32_t blink_selection;
volatile bool blink_now;

// LED pins
const uint32_t pin_led13_mask(1 << 27);
::Pio *const pin_led13_pio(PIOB);
const uint32_t pin_rx_mask(1 << 30);
::Pio *const pin_rx_pio(PIOC);
const uint32_t pin_tx_mask(1 << 21);
::Pio *const pin_tx_pio(PIOA);

/// @brief Enables a peripheral (e.g. PIO) clock
///
uint32_t pmc_enable_periph_clk(uint32_t ul_id) {
  if (ul_id > MAX_PERIPH_ID) {
    return 1;
  }

  if (ul_id < 32) {
    if ((PMC->PMC_PCSR0 & (1u << ul_id)) != (1u << ul_id)) {
      PMC->PMC_PCER0 = 1 << ul_id;
    }
  } else {
    ul_id -= 32;
    if ((PMC->PMC_PCSR1 & (1u << ul_id)) != (1u << ul_id)) {
      PMC->PMC_PCER1 = 1 << ul_id;
    }
  }

  return 0;
}

void pin_init_output(::Pio *port_n, uint32_t mask_n) {
  port_n->PIO_OWER = mask_n;   // Enable parallel port writes
  port_n->PIO_IDR = mask_n;    // Disable interrupt generation
  port_n->PIO_MDDR = mask_n;   // Disable multi driver (open drain) mode
  port_n->PIO_PUDR = mask_n;   // Disable pull up
  port_n->PIO_CODR = mask_n;   // Clear output value (low state)
  port_n->PIO_OER = mask_n;    // Enable port as output
  port_n->PIO_ABSR &= ~mask_n; // Select peripheral A
  port_n->PIO_PER = mask_n;    // Enable port
}

inline void pin_set_high(::Pio *pio_n, uint32_t pin_mask_n) {
  pio_n->PIO_SODR = pin_mask_n;
}

inline void pin_set_low(::Pio *pio_n, uint32_t pin_mask_n) {
  pio_n->PIO_CODR = pin_mask_n;
}

inline void pin_set(::Pio *pio_n, uint32_t pin_mask_n, bool flag_n) {
  if (flag_n) {
    pin_set_high(pio_n, pin_mask_n);
  } else {
    pin_set_low(pio_n, pin_mask_n);
  }
}

/// @brief Initializes some system components
void system_init() {
  SystemInit();

  // Reset blink variables
  blink_ticks_accu = 0;
  blink_selection = 0;
  blink_now = false;

  // Set Systick to 1ms interval, common to all SAM3 variants
  if (SysTick_Config(SystemCoreClock / 1000)) {
    while (true)
      ;
  }

  // Disable watchdog otherwise it will reboot the system
  // when we don't poll it frequently
  WDT->WDT_MR = WDT_MR_WDDIS;

  // Enable clocks for LED pins
  // All three used LED pins are on different PIOs
  pmc_enable_periph_clk(ID_PIOA);
  pmc_enable_periph_clk(ID_PIOB);
  pmc_enable_periph_clk(ID_PIOC);

  // Setup pins for output
  pin_init_output(pin_led13_pio, pin_led13_mask);
  pin_init_output(pin_rx_pio, pin_rx_mask);
  pin_init_output(pin_tx_pio, pin_tx_mask);

  // Turn off LEDs (RX and TX LEDs are inverted)
  pin_set_low(pin_led13_pio, pin_led13_mask);
  pin_set_high(pin_rx_pio, pin_rx_mask);
  pin_set_high(pin_tx_pio, pin_tx_mask);
}

inline void loop() {
  // blink_now is set in the interrupt handler SysTick_Handler
  if (blink_now) {
    blink_now = false;
    // Update LED states
    pin_set(pin_led13_pio, pin_led13_mask, (blink_selection == 0));
    // TX LED is inverted
    pin_set(pin_tx_pio, pin_tx_mask, !(blink_selection == 1));
    // RX LED is inverted
    pin_set(pin_rx_pio, pin_rx_mask, !(blink_selection == 2));
    // Increment blink selection and loop around at ( blink_selection == 3 )
    ++blink_selection;
    blink_selection %= 3;
  }
}

int main() {
  // System setup
  system_init();

  // Work loop
  while (true) {
    loop();
  }

  return 0;
}

/// @brief Handler for system tick interrupts
///
void SysTick_Handler() {
  ++blink_ticks_accu;
  if (blink_ticks_accu == blink_ticks) {
    blink_ticks_accu = 0;
    blink_now = true;
  }
}
