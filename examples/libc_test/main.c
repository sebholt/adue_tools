/* ----------------------------------------------------------------------------
 *         Cortex-M3 dummy handlers
 * ----------------------------------------------------------------------------
 * Copyright (c) 2014, Sebastian Holtermann
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following condition is met:
 *
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the disclaimer below.
 *
 * DISCLAIMER: THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 * BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDERS AND CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * ----------------------------------------------------------------------------
 */

#include <sam3x8e.h>
#include <stdio.h>
#include <string.h>

#define MAX_PERIPH_ID    44

// Blinking counter ticks
uint32_t loop_count;

// LED pins
const uint32_t pin_led13_mask = ( 1 << 27 );
Pio * const pin_led13_pio = PIOB;
const uint32_t pin_rx_mask = ( 1 << 30 );
Pio * const pin_rx_pio = PIOC;
const uint32_t pin_tx_mask = ( 1 << 21 );
Pio * const pin_tx_pio = PIOA;

/// @brief Enables a peripheral (e.g. PIO) clock
///
uint32_t
pmc_enable_periph_clk (
	uint32_t ul_id )
{
	if ( ul_id > MAX_PERIPH_ID ) {
		return 1;
	}

	if ( ul_id < 32 ) {
		if ( ( PMC->PMC_PCSR0 & (1u << ul_id) ) != ( 1u << ul_id ) ) {
			PMC->PMC_PCER0 = 1 << ul_id;
		}
	} else {
		ul_id -= 32;
		if ( ( PMC->PMC_PCSR1 & ( 1u << ul_id ) ) != ( 1u << ul_id ) ) {
			PMC->PMC_PCER1 = 1 << ul_id;
		}
	}

	return 0;
}

void
pin_init_output (
	Pio * port_n,
	uint32_t mask_n )
{
	port_n->PIO_OWER = mask_n; // Enable parallel port writes
	port_n->PIO_IDR = mask_n; // Disable interrupt generation
	port_n->PIO_MDDR = mask_n; // Disable multi driver (open drain) mode
	port_n->PIO_PUDR = mask_n; // Disable pull up
	port_n->PIO_CODR = mask_n; // Clear output value (low state)
	port_n->PIO_OER = mask_n; // Enable port as output
	port_n->PIO_ABSR &= ~mask_n; // Select peripheral A
	port_n->PIO_PER = mask_n; // Enable port
}

inline
void
pin_set_high (
	Pio * pio_n,
	uint32_t pin_mask_n )
{
	pio_n->PIO_SODR = pin_mask_n;
}

inline
void
pin_set_low (
	Pio * pio_n,
	uint32_t pin_mask_n )
{
	pio_n->PIO_CODR = pin_mask_n;
}

inline
void
pin_set (
	Pio * pio_n,
	uint32_t pin_mask_n,
	uint8_t flag_n )
{
	if ( flag_n != 0 ) {
		pin_set_high ( pio_n, pin_mask_n );
	} else {
		pin_set_low ( pio_n, pin_mask_n );
	}
}

/// @brief Initializes some system components
///
void
system_init ( void )
{
	SystemInit();

	// Reset blink variables
	loop_count = 0;

	// Set Systick to 1ms interval, common to all SAM3 variants
	if ( SysTick_Config ( SystemCoreClock / 1000 ) ) {
		while ( 1 );
	}

	// Disable watchdog otherwise it will reboot the system
	// when we don't poll it frequently
	WDT->WDT_MR = WDT_MR_WDDIS;

	// Enable clocks for LED pins
	// All three used LED pins are on different PIOs
	pmc_enable_periph_clk ( ID_PIOA );
	pmc_enable_periph_clk ( ID_PIOB );
	pmc_enable_periph_clk ( ID_PIOC );

	// Setup pins for output
	pin_init_output ( pin_led13_pio, pin_led13_mask );
	pin_init_output ( pin_rx_pio, pin_rx_mask );
	pin_init_output ( pin_tx_pio, pin_tx_mask );

	// Turn off LEDs (RX and TX LEDs are inverted)
	pin_set_low ( pin_led13_pio, pin_led13_mask );
	pin_set_high ( pin_rx_pio, pin_rx_mask );
	pin_set_high ( pin_tx_pio, pin_tx_mask );
}

inline
void
loop ( void )
{
	// Libc test
	{
		// Begin example string
		const char * src_str = "val_a: 526 val_b: 19 val_c: -35 val_d: 45.8 val_e: 12.5";
		struct Values {
			unsigned int val_a;
			int val_b;
			int val_c;
			float val_d;
			double val_e;
		} vals;

		const uint32_t str_buff_size = 512;
		char str[str_buff_size];

		const uint32_t val_buff_size = 32;
		char val_a_head[val_buff_size];
		char val_b_head[val_buff_size];
		char val_c_head[val_buff_size];
		char val_d_head[val_buff_size];
		char val_e_head[val_buff_size];

		// Copy source string
		strncpy ( str, src_str, str_buff_size );
		// Ensure zero terminations
		str[str_buff_size-1] = 0;

		const uint32_t num_loop = 1000;
		for ( uint32_t ii=0; ii != num_loop; ++ii ) {
			// Scan into binary buffers
			sscanf ( str, "%31s %u %31s %i %31s %i %31s %f %31s %lf",
				&val_a_head[0], &vals.val_a,
				&val_b_head[0], &vals.val_b,
				&val_c_head[0], &vals.val_c,
				&val_d_head[0], &vals.val_d,
				&val_e_head[0], &vals.val_e );
			// Write binary buffers back into string
			snprintf ( str, str_buff_size, "%s %u %s %i %s %i %s %f %s %lf",
				&val_a_head[0], vals.val_a,
				&val_b_head[0], vals.val_b,
				&val_c_head[0], vals.val_c,
				&val_d_head[0], vals.val_d,
				&val_e_head[0], vals.val_e );
		}
	}

	// Update LED states
	{
		// TX LED is inverted
		pin_set ( pin_tx_pio, pin_tx_mask, !( ( loop_count % 2 ) == 0 ) );
		// RX LED is inverted
		pin_set ( pin_rx_pio, pin_rx_mask, !( ( loop_count % 2 ) != 0 ) );
		// Increment blink selection and loop around at ( blink_selection == 3 )
		++loop_count;
	}
}

int
main ( void )
{
	// System setup
	system_init();

	// Work loop
	while ( 1 ) {
		loop();
	}

	return 0;
}
